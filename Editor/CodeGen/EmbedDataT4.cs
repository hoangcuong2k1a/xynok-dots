﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.DOTs.Controller.Core;
using Xynok.DOTs.Controller.Data.StateBonds.Core;
using Xynok.Enums;

namespace Xynok.DOTs.Editor.CodeGen
{
    [CreateAssetMenu(menuName = "Xynok/CodeGen/DOTs/EmbedDataT4")]
    public class EmbedDataT4 : ScriptableObject
    {
        // chỉ cần tham chiếu đến 1 kiểu dữ liệu bất kì nằm trong thư viện Zk1Core.Enums.dll là dc
        [ReadOnly] public FloatDataType floatDataType;
        public const int ChunkSize = 10;

        
        private DatabaseDots _databaseDots;
        public EntityBondDict EntityBondDict
        {
            get
            {
                _databaseDots = null;
                _databaseDots ??= FindObjectOfType<EntityDatabase>().database;
              
                return _databaseDots.GetEntityBondDict();
            }
        }

        public List<string[]> SplitToArray<T>(T type, int chunkSize) where T : Enum
        {
            if (chunkSize < ChunkSize) chunkSize = ChunkSize;

            string[] arr = Enum.GetNames(typeof(T));

            return SplitToArray(arr, chunkSize);
        }

        List<string[]> SplitToArray(string[] arr, int chunkSize = ChunkSize)
        {
            return arr.Select((value, index) => new { Index = index, Value = value })
                .GroupBy(x => x.Index / chunkSize)
                .Select(g => g.Select(x => x.Value).ToArray())
                .ToList();
        }
    }
}