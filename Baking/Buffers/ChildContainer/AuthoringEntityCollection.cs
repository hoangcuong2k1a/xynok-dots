﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Xynok.DOTs.Baking.Buffers.ChildContainer
{
   

    public struct EntityChildElement : IBufferElementData
    {
        public Entity Value;
        public float3 Offset;
    }

    [Serializable]
    public class EntityElementAuthoring
    {
        public Vector3 offset;
        public GameObject element;
    }

    /// <summary>
    /// store a collection of entities and their offsets to a source entity
    /// </summary>
    public class AuthoringEntityCollection : MonoBehaviour
    {
        [SerializeField] private EntityElementAuthoring[] entityElements;

        public class AuthoringEntityHolderBaker : Baker<AuthoringEntityCollection>
        {
            public override void Bake(AuthoringEntityCollection authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                if (authoring.entityElements is { Length: > 0 })
                {
                    var entityElements = AddBuffer<EntityChildElement>(entity);
                    for (int i = 0; i < authoring.entityElements.Length; i++)
                    {
                        entityElements.Add(new EntityChildElement
                        {
                            Value = GetEntity(authoring.entityElements[i].element, TransformUsageFlags.Dynamic),
                            Offset = authoring.entityElements[i].offset
                        });
                    }
                }
            }
        }
    }
}