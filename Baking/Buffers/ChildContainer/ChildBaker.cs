﻿using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Entities;

namespace Xynok.DOTs.Baking.Buffers.ChildContainer
{
    /// <summary>
    /// if this is a child baker, it will add an entity source component to the entity to reference the it's source
    /// </summary>
    public abstract class ChildBaker<T>: Baker<T> where T : MonoBehaviour
    {
        public override void Bake(T authoring)
        {
            var entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent(entity, new EntitySource());
        }
    }
}