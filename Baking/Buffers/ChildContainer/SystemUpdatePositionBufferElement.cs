﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Xynok.DOTs.Attributes.Systems;

namespace Xynok.DOTs.Baking.Buffers.ChildContainer
{
    [XynokSystem(XynokSystemAttribute.Main+"UpdatePositionBufferElement")]
    [UpdateBefore(typeof(TransformSystemGroup))]
    [RequireMatchingQueriesForUpdate]
    public partial struct SystemUpdatePositionBufferElement : ISystem
    {
        EntityQuery _entityElementQuery;

        public void OnCreate(ref SystemState state)
        {
            _entityElementQuery = state.GetEntityQuery(typeof(EntityChildElement));
        }

        public void OnUpdate(ref SystemState state)
        {
            var holderEntities = _entityElementQuery.ToEntityArray(Allocator.Temp);
            for (int i = 0; i < holderEntities.Length; i++)
            {
                var elements = state.EntityManager.GetBuffer<EntityChildElement>(holderEntities[i]);
                for (int j = 0; j < elements.Length; j++)
                {
                    var elementTrans = state.EntityManager.GetComponentData<LocalTransform>(elements[j].Value);
                    var sourceTrans = state.EntityManager.GetComponentData<LocalTransform>(holderEntities[i]);
                    elementTrans.Position = sourceTrans.Position + elements[j].Offset;
                    state.EntityManager.SetComponentData(elements[j].Value, elementTrans);
                }
            }
            
            holderEntities.Dispose();
        }
    }
}