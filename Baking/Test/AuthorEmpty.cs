﻿using Unity.Entities;
using UnityEngine;

namespace Xynok.DOTs.Baking.Test
{
    public class AuthorEmpty : MonoBehaviour
    {
        public class AuthorEmptyBaker : Baker<AuthorEmpty>
        {
            public override void Bake(AuthorEmpty authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
            }
        }
    }
}