﻿using System;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Dictionary;

namespace Xynok.DOTs.Baking.Core
{
    [Serializable]
    public class EntityAsPrefabDict : SerializableDictionary<ResourceID, GameObject>
    {
    }
}