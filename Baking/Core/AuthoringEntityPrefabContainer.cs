using Unity.Entities;
using UnityEngine;
using Xynok.Enums;

namespace Xynok.DOTs.Baking.Core
{



public struct EntityPrefabContainerComponent : IComponentData
    {
       
 public Entity PrefabNone;
   
   
 public Entity PrefabPlayer;
   
   
 public Entity PrefabSystem;
   
   
 public Entity PrefabBear;
   
   
 public Entity PrefabBird;
   
   
 public Entity PrefabFlowerPurple;
   
   
 public Entity PrefabCactus;
   
   
 public Entity PrefabMileStone;
   
   
        public Entity GetPrefab(ResourceID resourceID)
        {
            switch (resourceID)
            {
    
      case ResourceID.None:   return PrefabNone;
   

      case ResourceID.Player:   return PrefabPlayer;
   

      case ResourceID.System:   return PrefabSystem;
   

      case ResourceID.Bear:   return PrefabBear;
   

      case ResourceID.Bird:   return PrefabBird;
   

      case ResourceID.FlowerPurple:   return PrefabFlowerPurple;
   

      case ResourceID.Cactus:   return PrefabCactus;
   

      case ResourceID.MileStone:   return PrefabMileStone;
   
            }
  Debug.LogError($"{resourceID} does not exist");
            return default;
        }
    }

    

public class AuthoringEntityPrefabContainer : MonoBehaviour
    {
        public EntityAsPrefabDict entityAsPrefabDict;

        public class AuthoringPrefabContainerBaker : Baker<AuthoringEntityPrefabContainer>
        {
            public override void Bake(AuthoringEntityPrefabContainer authoring)
            {
                Entity entity = GetEntity(TransformUsageFlags.None);
          
  
                AddComponent(entity,
                    new EntityPrefabContainerComponent
                    {

    PrefabNone = authoring.entityAsPrefabDict.ContainsKey(ResourceID.None)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.None], TransformUsageFlags.Dynamic)
                            : Entity.Null,

    PrefabPlayer = authoring.entityAsPrefabDict.ContainsKey(ResourceID.Player)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.Player], TransformUsageFlags.Dynamic)
                            : Entity.Null,

    PrefabSystem = authoring.entityAsPrefabDict.ContainsKey(ResourceID.System)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.System], TransformUsageFlags.Dynamic)
                            : Entity.Null,

    PrefabBear = authoring.entityAsPrefabDict.ContainsKey(ResourceID.Bear)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.Bear], TransformUsageFlags.Dynamic)
                            : Entity.Null,

    PrefabBird = authoring.entityAsPrefabDict.ContainsKey(ResourceID.Bird)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.Bird], TransformUsageFlags.Dynamic)
                            : Entity.Null,

    PrefabFlowerPurple = authoring.entityAsPrefabDict.ContainsKey(ResourceID.FlowerPurple)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.FlowerPurple], TransformUsageFlags.Dynamic)
                            : Entity.Null,

    PrefabCactus = authoring.entityAsPrefabDict.ContainsKey(ResourceID.Cactus)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.Cactus], TransformUsageFlags.Dynamic)
                            : Entity.Null,

    PrefabMileStone = authoring.entityAsPrefabDict.ContainsKey(ResourceID.MileStone)
                            ? GetEntity(authoring.entityAsPrefabDict[ResourceID.MileStone], TransformUsageFlags.Dynamic)
                            : Entity.Null,
                    });
            }
        }
    }

}