﻿#if UNITY_EDITOR

using System.IO;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Utils;

namespace Xynok.DOTs.Baking.Editor
{
    [CreateAssetMenu(menuName = "Xynok/CodeGen/DOTs/AuthoringBakerGenerator")]
    public class AuthoringBakerGenerator : ScriptableObject
    {
        [SerializeField] private string generateAuthoringFolderPath = @"Packages/DOTs/Baking/Generated";

        [SerializeField] private string templateAuthoringFilePath =
            @"Packages/DOTs/Baking/Editor/AuthoringBakerTemplate.txt";

        [SerializeField] [ReadOnly] private string prefix = "Authoring";
        [SerializeField] private bool deleteAllExistedFile = true;

        [Button]
        public void GenAllAuthoring()
        {
            if (deleteAllExistedFile) DeleteAllExistedFile(generateAuthoringFolderPath);

            ResourceID[] resourceIds = EnumHelper.GetAllValuesOf<ResourceID>();
            for (int i = 0; i < resourceIds.Length; i++)
            {
                string name = $"{prefix}{resourceIds[i]}";
                string fileName = $"{name}.cs";
                string filePath = Path.Combine(generateAuthoringFolderPath, fileName);
                string fileContent = GetSourceComponentPair();
                fileContent = fileContent.Replace("CLASS_NAME", name);
                fileContent = fileContent.Replace("TAG_NAME", resourceIds[i].ToString());
                File.WriteAllText(filePath, fileContent);
            }

            AssetDatabase.Refresh();
        }

        void DeleteAllExistedFile(string path)
        {
            string[] files = Directory.GetFiles(path);
            bool insideAsset = path.Contains("Assets");
            foreach (string file in files)
            {
                if (insideAsset) AssetDatabase.DeleteAsset(file);
                else File.Delete(file);
            }

            if (insideAsset)
            {
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        string GetSourceComponentPair()
        {
            return File.ReadAllText(templateAuthoringFilePath);
        }
    }
}
#endif