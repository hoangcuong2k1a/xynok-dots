﻿using System;
using ALCCore.GUI.View.Btn.Core;
using UnityEngine;
using Xynok.APIs;
using Xynok.DOTs.Controller.Data;
using Xynok.OOP.Data.Core.Utils;
using Xynok.OOP.GUI.View.Texts.Core;

namespace ALCCore.DOTs.GUI.Entities
{
    public class EntityItemView : MonoBehaviour, IInjectable<ResourceActionableWrapperAsEntity>
    {
        [SerializeField] private TextBase nameTxt;
        [SerializeField] private ButtonBase btn;
        private ResourceActionableWrapperAsEntity _dependency;
        private Action<ResourceActionableWrapperAsEntity> _actionOnclick;

        private void Start()
        {
            btn.AddListener(Onclick);
        }

        public void SetDependency(ResourceActionableWrapperAsEntity dependency)
        {
            if (_dependency.IsValid()) Dispose();

            nameTxt.SetValue(
                $"{dependency.resourceID}({dependency.Entity.Version}, {dependency.Entity.Index})");
            _dependency = dependency;
            gameObject.SetActive(true);
        }

        void Onclick()
        {
            if (_dependency.IsValid()) _actionOnclick?.Invoke(_dependency);
        }

        public void SetActionOnclick(Action<ResourceActionableWrapperAsEntity> action)
        {
            _actionOnclick = action;
        }

        public void Dispose()
        {
            gameObject.SetActive(false);
            btn.RemoveListener(Onclick);
        }
    }
}