﻿using System.Collections.Generic;
using ALCCore.DOTs.GUI.Entities;
using UnityEngine;
using Xynok.DOTs.Controller.Data;
using Xynok.DOTs.Worlds;

namespace Xynok.DOTs.GUI.Entities
{
    public class DotsGUIEntityScroller : MonoBehaviour
    {
        [SerializeField] private EntityItemView prefab;
        [SerializeField] private Transform container;
        [SerializeField] private DotsGUIEntityFloatCompScroller floatCompScroller;
        [SerializeField] private DotsGUIEntityBoolCompScroller boolCompScroller;
        [SerializeField] private DotsGUIEntityTriggerCompScroller triggerCompScroller;
        private List<EntityItemView> _entityItemViews = new();

        private void Start()
        {
            Init();
            Binding();
        }

        void Init()
        {
        }

        void Binding()
        {
            WorldController.Instance.DatabaseDots.GetResourceActionableWrapperCollection().OnAdd += OnAddEntity;
        }

        void OnAddEntity(ResourceActionableWrapperAsEntity resourceActionableWrapperAsEntity)
        {
            EntityItemView entityItemView = Instantiate(prefab, container);
            entityItemView.SetDependency(resourceActionableWrapperAsEntity);
            entityItemView.SetActionOnclick(OnClickEntityView);
            _entityItemViews.Add(entityItemView);
            
        }

        void OnClickEntityView(ResourceActionableWrapperAsEntity resourceActionableWrapperAsEntity)
        {
            floatCompScroller.SetDependency(resourceActionableWrapperAsEntity);
            boolCompScroller.SetDependency(resourceActionableWrapperAsEntity);
            triggerCompScroller.SetDependency(resourceActionableWrapperAsEntity);
        }
    }
}