﻿using UnityEngine;
using Xynok.APIs;
using Xynok.DOTs.Controller.Data;
using Xynok.OOP.Data.Core.Utils;
using Xynok.OOP.GUI.View.Data;
using Zk1Core.Utils;

namespace Xynok.DOTs.GUI.Entities
{
    public class DotsGUIEntityTriggerCompScroller: MonoBehaviour, IInjectable<ResourceActionableWrapperAsEntity>
    {
        [SerializeField] private TriggerDataView prefab;
        [SerializeField] private Transform container;
        
        private ResourceActionableWrapperAsEntity _dependency;
        public void SetDependency(ResourceActionableWrapperAsEntity dependency)
        {
            if(_dependency.IsValid()) Dispose();
            _dependency = dependency;
            for (int i = 0; i < _dependency.triggers.Length; i++)
            {
                TriggerDataView dataView = Instantiate(prefab, container);
                dataView.SetDependency(_dependency.triggers[i]);
            }
        }

        public void Dispose()
        {
            container.ClearAllChild();
        }
    }
}