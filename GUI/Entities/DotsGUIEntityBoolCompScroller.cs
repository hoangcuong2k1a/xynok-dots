﻿using UnityEngine;
using Xynok.APIs;
using Xynok.DOTs.Controller.Data;
using Xynok.OOP.Data.Core.Utils;
using Xynok.OOP.GUI.View.Data;
using Zk1Core.Utils;

namespace Xynok.DOTs.GUI.Entities
{
    public class DotsGUIEntityBoolCompScroller: MonoBehaviour, IInjectable<ResourceActionableWrapperAsEntity>
    {
        [SerializeField] private BoolDataView prefab;
        [SerializeField] private Transform container;
        
        private ResourceActionableWrapperAsEntity _dependency;
        public void SetDependency(ResourceActionableWrapperAsEntity dependency)
        {
            if(_dependency.IsValid()) Dispose();
            _dependency = dependency;
            for (int i = 0; i < _dependency.states.Length; i++)
            {
                BoolDataView dataView = Instantiate(prefab, container);
                dataView.SetDependency(_dependency.states[i]);
            }
        }

        public void Dispose()
        {
            container.ClearAllChild();
        }
    }
}