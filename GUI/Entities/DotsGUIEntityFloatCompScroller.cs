﻿using UnityEngine;
using Xynok.APIs;
using Xynok.DOTs.Controller.Data;
using Xynok.OOP.Data.Core.Utils;
using Xynok.OOP.GUI.View.Data;
using Zk1Core.Utils;

namespace ALCCore.DOTs.GUI.Entities
{
    public class DotsGUIEntityFloatCompScroller: MonoBehaviour, IInjectable<ResourceActionableWrapperAsEntity>
    {
        [SerializeField] private FloatDataView prefab;
        [SerializeField] private Transform container;
        
        private ResourceActionableWrapperAsEntity _dependency;
        public void SetDependency(ResourceActionableWrapperAsEntity dependency)
        {
            if(_dependency.IsValid()) Dispose();
            _dependency = dependency;
            for (int i = 0; i < _dependency.stats.Length; i++)
            {
                FloatDataView floatDataView = Instantiate(prefab, container);
                floatDataView.SetDependency(_dependency.stats[i]);
            }
        }

        public void Dispose()
        {
            container.ClearAllChild();
        }
    }
}