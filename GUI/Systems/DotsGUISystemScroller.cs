﻿using System.Linq;
using UnityEngine;
using Xynok.DOTs.Attributes;
using Xynok.DOTs.Attributes.Systems;
using Xynok.DOTs.Systems.Groups;
using Xynok.DOTs.Worlds;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Utils;
using Zk1Core.Utils;

namespace Xynok.DOTs.GUI.Systems
{
    public class DotsGUISystemScroller : MonoBehaviour
    {
        [SerializeField] private SystemStatusItemView prefab;
        [SerializeField] private Transform container;

        private void Start()
        {
            InitSystemViews();
        }

        void InitSystemViews()
        {
            container.ClearAllChild();

            string[] systems = XynokSystemAttribute.GetAllSystemNames();

            systems.ToList().ForEach(system =>
                {
                    var systemStatusView = Instantiate(prefab, container);
                    systemStatusView.SetData(null, system, true);
                }
            );
        }
    }
}