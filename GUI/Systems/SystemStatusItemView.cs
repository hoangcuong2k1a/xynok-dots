﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Xynok.GUI.View.Toggles.Core;
using Xynok.OOP.GUI.View.Texts.Core;

namespace Xynok.DOTs.GUI.Systems
{
    [Serializable]
    public struct SystemViewStatusData
    {
        public Color background;
        public Color text;
        public string status;
    }

    public class SystemStatusItemView : MonoBehaviour
    {
        [SerializeField] private TextBase statusTxt;
        [SerializeField] private ToggleBase toggleBase;
        [SerializeField] private Image background;
        [SerializeField] private SystemViewStatusData activeStatus;
        [SerializeField] private SystemViewStatusData nonActiveStatus;


        public void SetData(Action<bool> onValueChanged, string label, bool isOn)
        {
            toggleBase.SetData(label, isOn);
            toggleBase.AddListener(onValueChanged);
            toggleBase.AddListener(ChangeColor);
            ChangeColor(isOn);
        }

        void ChangeColor(bool active)
        {
            background.color = active ? activeStatus.background : nonActiveStatus.background;
            statusTxt.SetValue(active ? activeStatus.status : nonActiveStatus.status);
            statusTxt.txt.color = active ? activeStatus.text : nonActiveStatus.text;
        }
    }
}