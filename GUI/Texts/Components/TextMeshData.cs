﻿using Unity.Collections;
using Unity.Entities;
using Xynok.Utils;

namespace Xynok.DOTs.GUI.Texts.Components
{
    public struct TextMeshData : IComponentData
    {
        public FixedString512Bytes Text;
        public FixedString512Bytes VisualText;

        private float _floatValue;
        public float CurrentFloat => _floatValue;

        public void SetFloatValue(float value)
        {
            _floatValue = value;
            Text = "";
            for (int i = 0; i < value; i++)
            {
                Text += VisualText.Value == string.Empty ? SymbolUtils.Heart : VisualText.Value;
            }
        }
    }
}