﻿using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Baking.Buffers.ChildContainer;
using Xynok.DOTs.GUI.Texts.Components;

namespace Xynok.DOTs.GUI.Texts.Core
{
    [RequireComponent(typeof(TextMesh))]
    public class TextMeshBase : MonoBehaviour
    {
        [ReadOnly] [SerializeField] private TextMesh textMesh;

        private void OnValidate()
        {
            if (!textMesh) textMesh = GetComponent<TextMesh>();
        }

        public class TextMeshBaseBaker : ChildBaker<TextMeshBase>
        {
            public override void Bake(TextMeshBase authoring)
            {
                base.Bake(authoring);
                Entity entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new TextMeshData { Text = authoring.textMesh.text });
            }
        }
    }
}