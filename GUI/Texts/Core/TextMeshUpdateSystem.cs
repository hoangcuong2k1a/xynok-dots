﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Xynok.DOTs.Attributes.Systems;
using Xynok.DOTs.GUI.Texts.Components;

namespace Xynok.DOTs.GUI.Texts.Core
{
    [RequireMatchingQueriesForUpdate]
    [UpdateAfter(typeof(TransformSystemGroup))]
    [XynokSystem(XynokSystemAttribute.Main + "TextMeshUpdateSystem")]
    public partial struct TextMeshUpdateSystem : ISystem
    {
        private EntityQuery _textMeshQuery;

        public void OnCreate(ref SystemState state)
        {
            _textMeshQuery = state.GetEntityQuery(typeof(TextMeshData), typeof(TextMesh));
        }

        public void OnUpdate(ref SystemState state)
        {
            var textEntities = _textMeshQuery.ToEntityArray(Allocator.Temp);
            for (int i = 0; i < textEntities.Length; i++)
            {
                var textMesh = state.EntityManager.GetComponentObject<TextMesh>(textEntities[i]);
                var textMeshData = state.EntityManager.GetComponentData<TextMeshData>(textEntities[i]);
                textMesh.text = $"{textMeshData.Text}";
            }
            textEntities.Dispose();
        }
    }
}