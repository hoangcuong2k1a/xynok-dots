﻿using Unity.Collections;
using Unity.Entities;
using Xynok.DOTs.Attributes.Systems;
using Xynok.DOTs.Entities;
using Xynok.DOTs.GUI.Texts.Components;
using Xynok.DOTs.GUI.Texts.EntityInfo.Components;
using Xynok.Enums;

namespace Xynok.DOTs.GUI.Texts.EntityInfo
{
    [XynokSystem(XynokSystemAttribute.Main + "TrackingEntityInfoSystem")]
    public partial struct TrackingEntityInfoSystem : ISystem
    {
        EntityQuery _entityInfoQuery;

        public void OnCreate(ref SystemState state)
        {
            _entityInfoQuery = state.GetEntityQuery(typeof(EntityInfoGuide), typeof(EntitySource));
        }

        public void OnUpdate(ref SystemState state)
        {
            var infoEntities = _entityInfoQuery.ToEntityArray(Allocator.Temp);
            var sourceEntities = _entityInfoQuery.ToComponentDataArray<EntitySource>(Allocator.Temp);
            var entityInfos = _entityInfoQuery.ToComponentDataArray<EntityInfoGuide>(Allocator.Temp);

            for (int i = 0; i < entityInfos.Length; i++)
            {
                if (entityInfos[i].dataType == DataType.Float)
                {
                    var textMeshData = state.EntityManager.GetComponentData<TextMeshData>(infoEntities[i]);
                    float value = entityInfos[i].GetFloatValue(ref state, sourceEntities[i].Source);
                    
                    if (value == textMeshData.CurrentFloat) continue;
                    
                    textMeshData.SetFloatValue(value);
                    state.EntityManager.SetComponentData(infoEntities[i], textMeshData);
                    continue;
                }


                if (entityInfos[i].dataType == DataType.Boolean)
                {
                    continue;
                }

                if (entityInfos[i].dataType == DataType.Trigger)
                {
                }
            }

            infoEntities.Dispose();
            entityInfos.Dispose();
            sourceEntities.Dispose();
        }
    }
}