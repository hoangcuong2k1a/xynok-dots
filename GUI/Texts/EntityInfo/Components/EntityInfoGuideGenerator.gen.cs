
using System;
using Sirenix.OdinInspector;
using Unity.Entities;
using Xynok.DOTs.Components.Generated;
using Xynok.Enums;


namespace Xynok.DOTs.GUI.Texts.EntityInfo.Components
{
    
 [Serializable]
    public struct EntityInfoGuide : IComponentData
    {
        public DataType dataType;
        [ShowIf("dataType", DataType.Float)] public FloatDataType floatDataType;
        [ShowIf("dataType", DataType.Boolean)] public BoolDataType boolDataType;
        [ShowIf("dataType", DataType.Trigger)] public TriggerType triggerType;
        

public float GetFloatValue(ref SystemState state, Entity entitySource)
        {

        switch (floatDataType)
            {
                     case FloatDataType.None:
                    return state.EntityManager.GetComponentData<ComponentFloatNone>(entitySource).Value;
         case FloatDataType.ID:
                    return state.EntityManager.GetComponentData<ComponentFloatID>(entitySource).Value;
         case FloatDataType.MoveSpeed:
                    return state.EntityManager.GetComponentData<ComponentFloatMoveSpeed>(entitySource).Value;
         case FloatDataType.JumpForce:
                    return state.EntityManager.GetComponentData<ComponentFloatJumpForce>(entitySource).Value;
         case FloatDataType.JumpHoldDuration:
                    return state.EntityManager.GetComponentData<ComponentFloatJumpHoldDuration>(entitySource).Value;
         case FloatDataType.JumpHoldForce:
                    return state.EntityManager.GetComponentData<ComponentFloatJumpHoldForce>(entitySource).Value;
         case FloatDataType.MaxJump:
                    return state.EntityManager.GetComponentData<ComponentFloatMaxJump>(entitySource).Value;
         case FloatDataType.RadiusCollisionCheck:
                    return state.EntityManager.GetComponentData<ComponentFloatRadiusCollisionCheck>(entitySource).Value;
         case FloatDataType.JumpsLeft:
                    return state.EntityManager.GetComponentData<ComponentFloatJumpsLeft>(entitySource).Value;
         case FloatDataType.Hp:
                    return state.EntityManager.GetComponentData<ComponentFloatHp>(entitySource).Value;
         case FloatDataType.MaxAttackCount:
                    return state.EntityManager.GetComponentData<ComponentFloatMaxAttackCount>(entitySource).Value;
         case FloatDataType.AttackCount:
                    return state.EntityManager.GetComponentData<ComponentFloatAttackCount>(entitySource).Value;
         case FloatDataType.AttackHoldDuration:
                    return state.EntityManager.GetComponentData<ComponentFloatAttackHoldDuration>(entitySource).Value;
         case FloatDataType.MaxHp:
                    return state.EntityManager.GetComponentData<ComponentFloatMaxHp>(entitySource).Value;
         case FloatDataType.Rage:
                    return state.EntityManager.GetComponentData<ComponentFloatRage>(entitySource).Value;
         case FloatDataType.MaxRage:
                    return state.EntityManager.GetComponentData<ComponentFloatMaxRage>(entitySource).Value;
         case FloatDataType.Cooldown:
                    return state.EntityManager.GetComponentData<ComponentFloatCooldown>(entitySource).Value;
         case FloatDataType.Damage:
                    return state.EntityManager.GetComponentData<ComponentFloatDamage>(entitySource).Value;
         case FloatDataType.AttackSpeed:
                    return state.EntityManager.GetComponentData<ComponentFloatAttackSpeed>(entitySource).Value;
         case FloatDataType.RandomMoveRange:
                    return state.EntityManager.GetComponentData<ComponentFloatRandomMoveRange>(entitySource).Value;
         case FloatDataType.IdleTime:
                    return state.EntityManager.GetComponentData<ComponentFloatIdleTime>(entitySource).Value;
         case FloatDataType.PulseForce:
                    return state.EntityManager.GetComponentData<ComponentFloatPulseForce>(entitySource).Value;
         case FloatDataType.CooldownStackCombo:
                    return state.EntityManager.GetComponentData<ComponentFloatCooldownStackCombo>(entitySource).Value;
         case FloatDataType.ImmortalCooldown:
                    return state.EntityManager.GetComponentData<ComponentFloatImmortalCooldown>(entitySource).Value;
         case FloatDataType.StunDuration:
                    return state.EntityManager.GetComponentData<ComponentFloatStunDuration>(entitySource).Value;
         case FloatDataType.DashDuration:
                    return state.EntityManager.GetComponentData<ComponentFloatDashDuration>(entitySource).Value;
         case FloatDataType.DashCooldown:
                    return state.EntityManager.GetComponentData<ComponentFloatDashCooldown>(entitySource).Value;
         case FloatDataType.DashPower:
                    return state.EntityManager.GetComponentData<ComponentFloatDashPower>(entitySource).Value;
       
            }

            return -999;
        }
    }
}
