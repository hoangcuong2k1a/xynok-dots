﻿using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.GUI.Texts.Components;
using Xynok.DOTs.GUI.Texts.EntityInfo.Bakings;
using Xynok.DOTs.GUI.Texts.EntityInfo.Components;
using Xynok.Utils;

namespace Xynok.DOTs.GUI.Texts.EntityInfo
{
    [RequireComponent(typeof(TextMesh))]
    public class EntityFloatInfoAuthoring : MonoBehaviour, IBakerEntityInfo
    {
        [ReadOnly] [SerializeField] private TextMesh textMesh;
        [SerializeField] private string visualText = SymbolUtils.Heart;
        [SerializeField] private EntityInfoGuide entityInfo;
        public EntityInfoGuide EntityInfoGuide => entityInfo;

        private void OnValidate()
        {
            if (!textMesh) textMesh = GetComponent<TextMesh>();
        }

        public class EntityInfoAuthoringBaker : BakerEntityInfo<EntityFloatInfoAuthoring>
        {
            public override void Bake(EntityFloatInfoAuthoring authoring)
            {
                base.Bake(authoring);
                Entity entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new TextMeshData
                {
                    Text = authoring.textMesh.text,
                    VisualText = authoring.visualText
                });
            }
        }
    }
}