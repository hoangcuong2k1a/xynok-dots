﻿using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Baking.Buffers.ChildContainer;
using Xynok.DOTs.GUI.Texts.EntityInfo.Components;

namespace Xynok.DOTs.GUI.Texts.EntityInfo.Bakings
{
    public interface IBakerEntityInfo
    {
        EntityInfoGuide EntityInfoGuide { get; }
    }

    public abstract class BakerEntityInfo<T> : ChildBaker<T> where T : MonoBehaviour, IBakerEntityInfo
    {
        public override void Bake(T authoring)
        {
            base.Bake(authoring);
            Entity entity = GetEntity(TransformUsageFlags.Dynamic);
            AddComponent(entity, authoring.EntityInfoGuide);
        }
    }
}