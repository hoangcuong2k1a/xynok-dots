﻿using Unity.Entities;
using Xynok.DOTs.Controller.Core;
using Xynok.Patterns.Singleton;

namespace Xynok.DOTs.Worlds
{
    /// <summary>
    /// supply world
    /// </summary>
    public class WorldController : ALazySingleton<WorldController>
    {
        public World World => World.DefaultGameObjectInjectionWorld;
        public EntityManager EntityManager => World.EntityManager;

        public DatabaseDots DatabaseDots
        {
            get
            {
                _databaseDots ??= EntityDatabase.Instance.database;

                return _databaseDots;
            }
        }

        private DatabaseDots _databaseDots;
    }
}