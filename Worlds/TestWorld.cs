﻿using Unity.Collections;
using Unity.Entities;

namespace ALCCore.DOTs.Worlds
{
    public class TestWorld: World
    {
        public TestWorld(string name, WorldFlags flags = WorldFlags.Simulation) : base(name, flags)
        {
        }

        public TestWorld(string name, WorldFlags flags, AllocatorManager.AllocatorHandle backingAllocatorHandle) : base(name, flags, backingAllocatorHandle)
        {
        }
    }
}