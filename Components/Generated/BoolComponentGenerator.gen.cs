using System;
using Unity.Entities;
using Xynok.DOTs.Components.APIs;

namespace Xynok.DOTs.Components.Generated
{

[Serializable]
public struct ComponentBooleanNone : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
       public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentBooleanMoveReady : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
       public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentBooleanMoving : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
       public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentBooleanTheGround : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
       public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentBooleanCanContain : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
       public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentBooleanSelected : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
       public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentBooleanStunning : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
       public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
}