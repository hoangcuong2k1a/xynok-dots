using System;
using Unity.Entities;
using Xynok.DOTs.Components.APIs;

namespace Xynok.DOTs.Components.Generated
{

[Serializable]
public struct ComponentTriggerNone : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
     public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentTriggerForceSit : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
     public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentTriggerForceFly : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
     public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentTriggerForceAttack : IComponentData,IBaseValue<bool>
{
        public bool baseValue;
        public bool lastValue;
        public bool currentValue;
        public bool BaseValue => baseValue;
        public bool LastValue => lastValue;
        
     public bool Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(bool value)
        {
            baseValue = value;
        }
}
}