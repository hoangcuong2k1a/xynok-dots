using Unity.Entities;

namespace Xynok.DOTs.Components.Generated
{

public struct TagComponentNone : IComponentData{ }
public struct TagComponentPlayer : IComponentData{ }
public struct TagComponentSystem : IComponentData{ }
public struct TagComponentBear : IComponentData{ }
public struct TagComponentBird : IComponentData{ }
public struct TagComponentFlowerPurple : IComponentData{ }
public struct TagComponentCactus : IComponentData{ }
public struct TagComponentMileStone : IComponentData{ }
public struct TagComponentWater : IComponentData{ }
}