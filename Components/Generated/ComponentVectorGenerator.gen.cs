using System;
using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Components.APIs;

namespace Xynok.DOTs.Components.Generated
{

// Vector2
[Serializable]
public struct ComponentVector2None : IComponentData,IBaseValue<Vector2>
{
        public Vector2 baseValue;
        public Vector2 lastValue;
        public Vector2 currentValue;
        public Vector2 BaseValue => baseValue;
        public Vector2 LastValue => lastValue;
        
       public Vector2 Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(Vector2 value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentVector2Position : IComponentData,IBaseValue<Vector2>
{
        public Vector2 baseValue;
        public Vector2 lastValue;
        public Vector2 currentValue;
        public Vector2 BaseValue => baseValue;
        public Vector2 LastValue => lastValue;
        
       public Vector2 Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(Vector2 value)
        {
            baseValue = value;
        }
}

// Vector3
[Serializable]
public struct ComponentVector3None : IComponentData,IBaseValue<Vector3>
{
        public Vector3 baseValue;
        public Vector3 lastValue;
        public Vector3 currentValue;
        public Vector3 BaseValue => baseValue;
        public Vector3 LastValue => lastValue;
        
       public Vector3 Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(Vector3 value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentVector3Position : IComponentData,IBaseValue<Vector3>
{
        public Vector3 baseValue;
        public Vector3 lastValue;
        public Vector3 currentValue;
        public Vector3 BaseValue => baseValue;
        public Vector3 LastValue => lastValue;
        
       public Vector3 Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(Vector3 value)
        {
            baseValue = value;
        }
}

}