using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Baking.Buffers.ChildContainer;
using Xynok.DOTs.Entities;
using Xynok.DOTs.Entities.Generated;
using Xynok.DOTs.Worlds;
using Xynok.Enums;
using Xynok.OOP.Data.Core.SubModules;
using Xynok.OOP.Data.Primitives;


namespace Xynok.DOTs.Components.Generated
{
     
    public static class FactoryEntityComponent
    {
        private static EntityManager EntityManager => WorldController.Instance.World.EntityManager;

        public static void FillUpDataFor(Entity entity, ResourceID resourceID)
        {
            ResourceActionable resource = WorldController.Instance.DatabaseDots.GetEntityData(resourceID);
            if (resource == null)
            {
                Debug.LogError($"Doest not exist data of {resourceID}.");
                return;
            }
            EntityManager.AddComponentData(entity, new EntityTag(resourceID));
            AddComponentTo(entity, resource.stats);
            AddComponentTo(entity, resource.states);
            AddComponentTo(entity, resource.triggers);
            AddComponentTo(entity, resource.vector2Data);
            AddComponentTo(entity, resource.vector3Data);
             SpawnEntityCollection(entity);

            if (resource.objType == ObjType.Entity) AddTagAbilityTo(entity, resource.systemAsAbilities);
            
        }

static void SpawnEntityCollection(Entity entity)
        {
            bool hasHoldBuffer = EntityManager.HasBuffer<EntityChildElement>(entity);
            if(!hasHoldBuffer) return;
            var entityHolders = EntityManager.GetBuffer<EntityChildElement>(entity);
            for (int i = 0; i < entityHolders.Length; i++)
            {
               var e= EntityManager.Instantiate(entityHolders[i].Value);
               entityHolders[i] = new EntityChildElement
               {
                   Value = e,
                   Offset = entityHolders[i].Offset
               };
    EntityManager.SetComponentData(e, new EntitySource { Source = entity });
            }
        }

        static void LogRed(string message)
        {
            Debug.LogError(message);
        }


public static void SetComponent(Entity entity, TriggerData triggerData)
        {
            switch (triggerData.Key)
            {
                 case TriggerType.None:
                    if (!EntityManager.HasComponent<ComponentTriggerNone>(entity))
                    {
                        LogRed($"Does not exist trigger component of {triggerData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentTriggerNone
                    {
                        baseValue = triggerData.BaseValue,
                        lastValue = triggerData.LastValue,
                        currentValue = triggerData.Value,
                    });
                    break;         
                    case TriggerType.ForceSit:
                    if (!EntityManager.HasComponent<ComponentTriggerForceSit>(entity))
                    {
                        LogRed($"Does not exist trigger component of {triggerData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentTriggerForceSit
                    {
                        baseValue = triggerData.BaseValue,
                        lastValue = triggerData.LastValue,
                        currentValue = triggerData.Value,
                    });
                    break;         
                    case TriggerType.ForceFly:
                    if (!EntityManager.HasComponent<ComponentTriggerForceFly>(entity))
                    {
                        LogRed($"Does not exist trigger component of {triggerData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentTriggerForceFly
                    {
                        baseValue = triggerData.BaseValue,
                        lastValue = triggerData.LastValue,
                        currentValue = triggerData.Value,
                    });
                    break;         
                    case TriggerType.ForceAttack:
                    if (!EntityManager.HasComponent<ComponentTriggerForceAttack>(entity))
                    {
                        LogRed($"Does not exist trigger component of {triggerData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentTriggerForceAttack
                    {
                        baseValue = triggerData.BaseValue,
                        lastValue = triggerData.LastValue,
                        currentValue = triggerData.Value,
                    });
                    break;         
    
              
            }
        }


public static void SetComponent(Entity entity, Vector3Data vectorData)
        {
            switch (vectorData.Key)
            {
                 case Vector3DataType.None:
                    if (!EntityManager.HasComponent<ComponentVector3None>(entity))
                    {
                        LogRed($"Does not exist vector3 component of {vectorData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentVector3None
                    {
                        baseValue = vectorData.BaseValue,
                        lastValue = vectorData.LastValue,
                        currentValue = vectorData.Value,
                    });
                    break;         
                    case Vector3DataType.Position:
                    if (!EntityManager.HasComponent<ComponentVector3Position>(entity))
                    {
                        LogRed($"Does not exist vector3 component of {vectorData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentVector3Position
                    {
                        baseValue = vectorData.BaseValue,
                        lastValue = vectorData.LastValue,
                        currentValue = vectorData.Value,
                    });
                    break;         
    
              
            }
        }

public static void SetComponent(Entity entity, Vector2Data vectorData)
        {
            switch (vectorData.Key)
            {
                 case Vector2DataType.None:
                    if (!EntityManager.HasComponent<ComponentVector2None>(entity))
                    {
                        LogRed($"Does not exist vector2 component of {vectorData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentVector2None
                    {
                        baseValue = vectorData.BaseValue,
                        lastValue = vectorData.LastValue,
                        currentValue = vectorData.Value,
                    });
                    break;         
                    case Vector2DataType.Position:
                    if (!EntityManager.HasComponent<ComponentVector2Position>(entity))
                    {
                        LogRed($"Does not exist vector2 component of {vectorData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentVector2Position
                    {
                        baseValue = vectorData.BaseValue,
                        lastValue = vectorData.LastValue,
                        currentValue = vectorData.Value,
                    });
                    break;         
    
              
            }
        }


    public static void SetComponent(Entity entity, FloatData floatData)
        {
            switch (floatData.Key)
            {
                 case FloatDataType.None:
                    if (!EntityManager.HasComponent<ComponentFloatNone>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatNone
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.ID:
                    if (!EntityManager.HasComponent<ComponentFloatID>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatID
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.MoveSpeed:
                    if (!EntityManager.HasComponent<ComponentFloatMoveSpeed>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatMoveSpeed
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.JumpForce:
                    if (!EntityManager.HasComponent<ComponentFloatJumpForce>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatJumpForce
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.JumpHoldDuration:
                    if (!EntityManager.HasComponent<ComponentFloatJumpHoldDuration>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatJumpHoldDuration
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.JumpHoldForce:
                    if (!EntityManager.HasComponent<ComponentFloatJumpHoldForce>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatJumpHoldForce
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.MaxJump:
                    if (!EntityManager.HasComponent<ComponentFloatMaxJump>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatMaxJump
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.RadiusCollisionCheck:
                    if (!EntityManager.HasComponent<ComponentFloatRadiusCollisionCheck>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatRadiusCollisionCheck
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.JumpsLeft:
                    if (!EntityManager.HasComponent<ComponentFloatJumpsLeft>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatJumpsLeft
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.Hp:
                    if (!EntityManager.HasComponent<ComponentFloatHp>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatHp
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.MaxAttackCount:
                    if (!EntityManager.HasComponent<ComponentFloatMaxAttackCount>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatMaxAttackCount
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.AttackCount:
                    if (!EntityManager.HasComponent<ComponentFloatAttackCount>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatAttackCount
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.AttackHoldDuration:
                    if (!EntityManager.HasComponent<ComponentFloatAttackHoldDuration>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatAttackHoldDuration
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.MaxHp:
                    if (!EntityManager.HasComponent<ComponentFloatMaxHp>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatMaxHp
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.Rage:
                    if (!EntityManager.HasComponent<ComponentFloatRage>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatRage
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.MaxRage:
                    if (!EntityManager.HasComponent<ComponentFloatMaxRage>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatMaxRage
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.Cooldown:
                    if (!EntityManager.HasComponent<ComponentFloatCooldown>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatCooldown
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.Damage:
                    if (!EntityManager.HasComponent<ComponentFloatDamage>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatDamage
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.AttackSpeed:
                    if (!EntityManager.HasComponent<ComponentFloatAttackSpeed>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatAttackSpeed
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.RandomMoveRange:
                    if (!EntityManager.HasComponent<ComponentFloatRandomMoveRange>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatRandomMoveRange
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.IdleTime:
                    if (!EntityManager.HasComponent<ComponentFloatIdleTime>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatIdleTime
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.PulseForce:
                    if (!EntityManager.HasComponent<ComponentFloatPulseForce>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatPulseForce
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.CooldownStackCombo:
                    if (!EntityManager.HasComponent<ComponentFloatCooldownStackCombo>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatCooldownStackCombo
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.ImmortalCooldown:
                    if (!EntityManager.HasComponent<ComponentFloatImmortalCooldown>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatImmortalCooldown
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.StunDuration:
                    if (!EntityManager.HasComponent<ComponentFloatStunDuration>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatStunDuration
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.DashDuration:
                    if (!EntityManager.HasComponent<ComponentFloatDashDuration>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatDashDuration
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.DashCooldown:
                    if (!EntityManager.HasComponent<ComponentFloatDashCooldown>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatDashCooldown
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
                    case FloatDataType.DashPower:
                    if (!EntityManager.HasComponent<ComponentFloatDashPower>(entity))
                    {
                        LogRed($"Does not exist float component of {floatData.Key}.");
                        return;
                    }
                    EntityManager.SetComponentData(entity, new ComponentFloatDashPower
                    {
                        baseValue = floatData.BaseValue,
                        lastValue = floatData.LastValue,
                        currentValue = floatData.Value,
                    });
                    break;         
    
              
            }
        }

        public static void SetComponent(Entity entity, BoolData boolData)
        {
            switch (boolData.Key)
            {
                 case BoolDataType.None:
                    if (!EntityManager.HasComponent<ComponentBooleanNone>(entity))
                    {
                        LogRed($"Does not exist boolean component of {boolData.Key}.");
                        return;
                    }
    
                    EntityManager.SetComponentData(entity, new ComponentBooleanNone
                    {
                        baseValue = boolData.BaseValue,
                        lastValue = boolData.LastValue,
                        currentValue = boolData.Value,

                    });
                    break;         
                    case BoolDataType.MoveReady:
                    if (!EntityManager.HasComponent<ComponentBooleanMoveReady>(entity))
                    {
                        LogRed($"Does not exist boolean component of {boolData.Key}.");
                        return;
                    }
    
                    EntityManager.SetComponentData(entity, new ComponentBooleanMoveReady
                    {
                        baseValue = boolData.BaseValue,
                        lastValue = boolData.LastValue,
                        currentValue = boolData.Value,

                    });
                    break;         
                    case BoolDataType.Moving:
                    if (!EntityManager.HasComponent<ComponentBooleanMoving>(entity))
                    {
                        LogRed($"Does not exist boolean component of {boolData.Key}.");
                        return;
                    }
    
                    EntityManager.SetComponentData(entity, new ComponentBooleanMoving
                    {
                        baseValue = boolData.BaseValue,
                        lastValue = boolData.LastValue,
                        currentValue = boolData.Value,

                    });
                    break;         
                    case BoolDataType.TheGround:
                    if (!EntityManager.HasComponent<ComponentBooleanTheGround>(entity))
                    {
                        LogRed($"Does not exist boolean component of {boolData.Key}.");
                        return;
                    }
    
                    EntityManager.SetComponentData(entity, new ComponentBooleanTheGround
                    {
                        baseValue = boolData.BaseValue,
                        lastValue = boolData.LastValue,
                        currentValue = boolData.Value,

                    });
                    break;         
                    case BoolDataType.CanContain:
                    if (!EntityManager.HasComponent<ComponentBooleanCanContain>(entity))
                    {
                        LogRed($"Does not exist boolean component of {boolData.Key}.");
                        return;
                    }
    
                    EntityManager.SetComponentData(entity, new ComponentBooleanCanContain
                    {
                        baseValue = boolData.BaseValue,
                        lastValue = boolData.LastValue,
                        currentValue = boolData.Value,

                    });
                    break;         
                    case BoolDataType.Selected:
                    if (!EntityManager.HasComponent<ComponentBooleanSelected>(entity))
                    {
                        LogRed($"Does not exist boolean component of {boolData.Key}.");
                        return;
                    }
    
                    EntityManager.SetComponentData(entity, new ComponentBooleanSelected
                    {
                        baseValue = boolData.BaseValue,
                        lastValue = boolData.LastValue,
                        currentValue = boolData.Value,

                    });
                    break;         
                    case BoolDataType.Stunning:
                    if (!EntityManager.HasComponent<ComponentBooleanStunning>(entity))
                    {
                        LogRed($"Does not exist boolean component of {boolData.Key}.");
                        return;
                    }
    
                    EntityManager.SetComponentData(entity, new ComponentBooleanStunning
                    {
                        baseValue = boolData.BaseValue,
                        lastValue = boolData.LastValue,
                        currentValue = boolData.Value,

                    });
                    break;         
    
              
            }
        }

        static void AddTagAbilityTo(Entity entity, SystemAsAbilityCollection abilityCollection)
        {
            for (int i = 0; i < abilityCollection.Count; i++)
            {
                switch (abilityCollection[i])
                {
                  
                           case SystemAsAbilityType.TestSystem:
                        EntityManager.AddComponentData(entity, new EntityTagTestSystem());
                        break;
                                              case SystemAsAbilityType.MoveFlatWithInput:
                        EntityManager.AddComponentData(entity, new EntityTagMoveFlatWithInput());
                        break;
                                              case SystemAsAbilityType.BindingComponentToRealWorld:
                        EntityManager.AddComponentData(entity, new EntityTagBindingComponentToRealWorld());
                        break;
                                              case SystemAsAbilityType.MoveAsPath:
                        EntityManager.AddComponentData(entity, new EntityTagMoveAsPath());
                        break;
                                    }
            }
        }


    static   void AddComponentTo(Entity entity, Vector2Data[] floats)
        {
            for (int i = 0; i < floats.Length; i++)
            {
                switch (floats[i].Key)
                {
                                                case Vector2DataType.None:
                                EntityManager.AddComponentData(entity, new ComponentVector2None
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                            
                        });
                                break;
                                                case Vector2DataType.Position:
                                EntityManager.AddComponentData(entity, new ComponentVector2Position
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                            
                        });
                                break;
                                    }
            }
        }


 static   void AddComponentTo(Entity entity, TriggerData[] triggers)
        {
            for (int i = 0; i < triggers.Length; i++)
            {
                switch (triggers[i].Key)
                {
                                                case TriggerType.None:
                                EntityManager.AddComponentData(entity, new ComponentTriggerNone
                        {
                            baseValue = triggers[i].BaseValue,
                            lastValue = triggers[i].LastValue,
                            currentValue =triggers[i].Value,
                        });
                                break;
                                                case TriggerType.ForceSit:
                                EntityManager.AddComponentData(entity, new ComponentTriggerForceSit
                        {
                            baseValue = triggers[i].BaseValue,
                            lastValue = triggers[i].LastValue,
                            currentValue =triggers[i].Value,
                        });
                                break;
                                                case TriggerType.ForceFly:
                                EntityManager.AddComponentData(entity, new ComponentTriggerForceFly
                        {
                            baseValue = triggers[i].BaseValue,
                            lastValue = triggers[i].LastValue,
                            currentValue =triggers[i].Value,
                        });
                                break;
                                                case TriggerType.ForceAttack:
                                EntityManager.AddComponentData(entity, new ComponentTriggerForceAttack
                        {
                            baseValue = triggers[i].BaseValue,
                            lastValue = triggers[i].LastValue,
                            currentValue =triggers[i].Value,
                        });
                                break;
                                    }
            }
        }


    static   void AddComponentTo(Entity entity, Vector3Data[] floats)
        {
            for (int i = 0; i < floats.Length; i++)
            {
                switch (floats[i].Key)
                {
                                                case Vector3DataType.None:
                                EntityManager.AddComponentData(entity, new ComponentVector3None
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case Vector3DataType.Position:
                                EntityManager.AddComponentData(entity, new ComponentVector3Position
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                    }
            }
        }


         static   void AddComponentTo(Entity entity, FloatData[] floats)
        {
            for (int i = 0; i < floats.Length; i++)
            {
                switch (floats[i].Key)
                {
                                                case FloatDataType.None:
                                EntityManager.AddComponentData(entity, new ComponentFloatNone
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.ID:
                                EntityManager.AddComponentData(entity, new ComponentFloatID
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.MoveSpeed:
                                EntityManager.AddComponentData(entity, new ComponentFloatMoveSpeed
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.JumpForce:
                                EntityManager.AddComponentData(entity, new ComponentFloatJumpForce
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.JumpHoldDuration:
                                EntityManager.AddComponentData(entity, new ComponentFloatJumpHoldDuration
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.JumpHoldForce:
                                EntityManager.AddComponentData(entity, new ComponentFloatJumpHoldForce
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.MaxJump:
                                EntityManager.AddComponentData(entity, new ComponentFloatMaxJump
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.RadiusCollisionCheck:
                                EntityManager.AddComponentData(entity, new ComponentFloatRadiusCollisionCheck
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.JumpsLeft:
                                EntityManager.AddComponentData(entity, new ComponentFloatJumpsLeft
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.Hp:
                                EntityManager.AddComponentData(entity, new ComponentFloatHp
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.MaxAttackCount:
                                EntityManager.AddComponentData(entity, new ComponentFloatMaxAttackCount
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.AttackCount:
                                EntityManager.AddComponentData(entity, new ComponentFloatAttackCount
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.AttackHoldDuration:
                                EntityManager.AddComponentData(entity, new ComponentFloatAttackHoldDuration
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.MaxHp:
                                EntityManager.AddComponentData(entity, new ComponentFloatMaxHp
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.Rage:
                                EntityManager.AddComponentData(entity, new ComponentFloatRage
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.MaxRage:
                                EntityManager.AddComponentData(entity, new ComponentFloatMaxRage
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.Cooldown:
                                EntityManager.AddComponentData(entity, new ComponentFloatCooldown
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.Damage:
                                EntityManager.AddComponentData(entity, new ComponentFloatDamage
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.AttackSpeed:
                                EntityManager.AddComponentData(entity, new ComponentFloatAttackSpeed
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.RandomMoveRange:
                                EntityManager.AddComponentData(entity, new ComponentFloatRandomMoveRange
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.IdleTime:
                                EntityManager.AddComponentData(entity, new ComponentFloatIdleTime
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.PulseForce:
                                EntityManager.AddComponentData(entity, new ComponentFloatPulseForce
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.CooldownStackCombo:
                                EntityManager.AddComponentData(entity, new ComponentFloatCooldownStackCombo
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.ImmortalCooldown:
                                EntityManager.AddComponentData(entity, new ComponentFloatImmortalCooldown
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.StunDuration:
                                EntityManager.AddComponentData(entity, new ComponentFloatStunDuration
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.DashDuration:
                                EntityManager.AddComponentData(entity, new ComponentFloatDashDuration
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.DashCooldown:
                                EntityManager.AddComponentData(entity, new ComponentFloatDashCooldown
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                                case FloatDataType.DashPower:
                                EntityManager.AddComponentData(entity, new ComponentFloatDashPower
                        {
                            baseValue = floats[i].BaseValue,
                            lastValue = floats[i].LastValue,
                            currentValue =floats[i].Value,
                        });
                                break;
                                    }
            }
        }

   static void AddComponentTo(Entity entity, BoolData[] bools)
        {
            for (int i = 0; i < bools.Length; i++)
            {
                switch (bools[i].Key)
                {
                                                case BoolDataType.None:
                                EntityManager.AddComponentData(entity, new ComponentBooleanNone
{
                                    baseValue = bools[i].BaseValue,
                                    lastValue = bools[i].LastValue,
                                        currentValue =bools[i].Value,
                                    });
                                break;
                                                case BoolDataType.MoveReady:
                                EntityManager.AddComponentData(entity, new ComponentBooleanMoveReady
{
                                    baseValue = bools[i].BaseValue,
                                    lastValue = bools[i].LastValue,
                                        currentValue =bools[i].Value,
                                    });
                                break;
                                                case BoolDataType.Moving:
                                EntityManager.AddComponentData(entity, new ComponentBooleanMoving
{
                                    baseValue = bools[i].BaseValue,
                                    lastValue = bools[i].LastValue,
                                        currentValue =bools[i].Value,
                                    });
                                break;
                                                case BoolDataType.TheGround:
                                EntityManager.AddComponentData(entity, new ComponentBooleanTheGround
{
                                    baseValue = bools[i].BaseValue,
                                    lastValue = bools[i].LastValue,
                                        currentValue =bools[i].Value,
                                    });
                                break;
                                                case BoolDataType.CanContain:
                                EntityManager.AddComponentData(entity, new ComponentBooleanCanContain
{
                                    baseValue = bools[i].BaseValue,
                                    lastValue = bools[i].LastValue,
                                        currentValue =bools[i].Value,
                                    });
                                break;
                                                case BoolDataType.Selected:
                                EntityManager.AddComponentData(entity, new ComponentBooleanSelected
{
                                    baseValue = bools[i].BaseValue,
                                    lastValue = bools[i].LastValue,
                                        currentValue =bools[i].Value,
                                    });
                                break;
                                                case BoolDataType.Stunning:
                                EntityManager.AddComponentData(entity, new ComponentBooleanStunning
{
                                    baseValue = bools[i].BaseValue,
                                    lastValue = bools[i].LastValue,
                                        currentValue =bools[i].Value,
                                    });
                                break;
                                    }
            }
        }
    }
}
