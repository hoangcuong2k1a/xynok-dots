using System;
using Unity.Entities;
using Xynok.DOTs.Components.APIs;

namespace Xynok.DOTs.Components.Generated
{

[Serializable]
public struct ComponentFloatNone : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatID : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatMoveSpeed : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatJumpForce : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatJumpHoldDuration : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatJumpHoldForce : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatMaxJump : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatRadiusCollisionCheck : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatJumpsLeft : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatHp : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatMaxAttackCount : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatAttackCount : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatAttackHoldDuration : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatMaxHp : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatRage : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatMaxRage : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatCooldown : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatDamage : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatAttackSpeed : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatRandomMoveRange : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatIdleTime : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatPulseForce : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatCooldownStackCombo : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatImmortalCooldown : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatStunDuration : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatDashDuration : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatDashCooldown : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
[Serializable]
public struct ComponentFloatDashPower : IComponentData,IBaseValue<float>
{
        public float baseValue;
        public float lastValue;
        public float currentValue; 
        public float BaseValue => baseValue;
        public float LastValue => lastValue;
        
     public float Value // current value
        {
            get => currentValue;
                       set
                       {
                           lastValue = currentValue;
                           currentValue = value;
                       }
        }

        public void SetBaseValue(float value)
        {
            baseValue = value;
        }
}
}