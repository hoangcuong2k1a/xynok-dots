﻿namespace Xynok.DOTs.Components.APIs
{
    public interface IBaseValue<T>
    {
        T BaseValue { get; }
        void SetBaseValue(T value);
    }
}