﻿using System;
using Unity.Entities;
using Xynok.Enums;
using Xynok.Patterns.Factory.Reflection;

namespace Xynok.DOTs.Components.APIs
{
  
    public interface IComponentPairData<out T1, T2> : IComponentData, ITypeOnFactory<T1> where T1 : Enum
    {
        T1 Key { get; }
        T2 Value { get; set; }
    }


    public interface IFloatComponent : IComponentPairData<FloatDataType, float>
    {
    }

    public interface IBooComponent : IComponentPairData<BoolDataType, bool>
    {
    }
}