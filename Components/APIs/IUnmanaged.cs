﻿namespace Xynok.DOTs.Components.APIs
{
    public interface IUnmanaged<out T> where T : struct
    {
        T Self { get; }
    }

    public interface IUnmanagedComponent
    {
        T GetSelf<T>() where T :struct, IUnmanaged<T>; 
    }
}