using Unity.Entities;
using Unity.Jobs;
using Xynok.DOTs.Systems.Groups;
using Xynok.DOTs.Attributes.Systems;
namespace Xynok.DOTs.Systems.Core.StateBonds
{

// STATE BONDS ......................................

     [UpdateInGroup(typeof(XynokSimulationSystemGroup))]
 [RequireMatchingQueriesForUpdate]
  [XynokSystem(XynokSystemAttribute.Main+"SystemStateBondForBird")]
  public partial class SystemStateBondForBird : SystemBase{

 protected override void OnUpdate()
        {

StateBondJobBirdRootMovingTrue jobBirdRootMovingTrue = new ();
StateBondJobBirdRootTheGroundTrue jobBirdRootTheGroundTrue = new ();
StateBondJobBirdRootStunningTrue jobBirdRootStunningTrue = new ();


var jobHandle0 = jobBirdRootMovingTrue.ScheduleParallel(Dependency);
var jobHandle1 = jobBirdRootTheGroundTrue.ScheduleParallel(jobHandle0);
var jobHandle2 = jobBirdRootStunningTrue.ScheduleParallel(jobHandle1);

Dependency= JobHandle.CombineDependencies(
  jobHandle0
, jobHandle1
, jobHandle2

);
}
}




     [UpdateInGroup(typeof(XynokSimulationSystemGroup))]
 [RequireMatchingQueriesForUpdate]
  [XynokSystem(XynokSystemAttribute.Main+"SystemStateBondForBear")]
  public partial class SystemStateBondForBear : SystemBase{

 protected override void OnUpdate()
        {

StateBondJobBearRootStunningTrue jobBearRootStunningTrue = new ();

Dependency= jobBearRootStunningTrue.ScheduleParallel(Dependency);


}
}





// TRIGGER BONDS ......................................

     [UpdateInGroup(typeof(XynokSimulationSystemGroup))]
 [RequireMatchingQueriesForUpdate]
 [XynokSystem(XynokSystemAttribute.Main+"SystemTriggerStateBondForBird")]
  public partial class SystemTriggerStateBondForBird : SystemBase{

 protected override void OnUpdate()
        {

StateBondJobBirdRootTriggerForceSit jobBirdRootForceSit = new ();
StateBondJobBirdRootTriggerForceFly jobBirdRootForceFly = new ();





var jobHandle0 = jobBirdRootForceSit.ScheduleParallel(Dependency);



var jobHandle1 = jobBirdRootForceFly.ScheduleParallel(jobHandle0);

Dependency= JobHandle.CombineDependencies(
  jobHandle0
, jobHandle1

);


}
}




     [UpdateInGroup(typeof(XynokSimulationSystemGroup))]
 [RequireMatchingQueriesForUpdate]
 [XynokSystem(XynokSystemAttribute.Main+"SystemTriggerStateBondForBear")]
  public partial class SystemTriggerStateBondForBear : SystemBase{

 protected override void OnUpdate()
        {

StateBondJobBearRootTriggerForceFly jobBearRootForceFly = new ();

Dependency= jobBearRootForceFly.ScheduleParallel(Dependency);

}
}




}