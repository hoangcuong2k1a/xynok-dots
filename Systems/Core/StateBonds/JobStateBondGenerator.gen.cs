using Unity.Entities;
using Unity.Burst;
using Xynok.DOTs.Components.Generated;
using Xynok.DOTs.Entities;
using Xynok.Enums;

namespace Xynok.DOTs.Systems.Core.StateBonds
{

// STATE BONDS ......................................


[BurstCompile]
   public partial struct StateBondJobBirdRootMovingTrue : IJobEntity
{
 
        void Execute(in EntityTag entityTag, ref ComponentBooleanMoving root
,ref ComponentBooleanCanContain boolCanContain     
)
{

          if (entityTag.Name!=ResourceID.Bird) return;
        
          bool canMakeImpact = root.Value==  true ;

        if(canMakeImpact){

            boolCanContain.Value = true; 

}

}

}
[BurstCompile]
   public partial struct StateBondJobBirdRootTheGroundTrue : IJobEntity
{
 
        void Execute(in EntityTag entityTag, ref ComponentBooleanTheGround root
,ref ComponentBooleanMoveReady boolMoveReady     
)
{

          if (entityTag.Name!=ResourceID.Bird) return;
        
          bool canMakeImpact = root.Value==  true ;

        if(canMakeImpact){

            boolMoveReady.Value = true; 

}

}

}
[BurstCompile]
   public partial struct StateBondJobBirdRootStunningTrue : IJobEntity
{
 
        void Execute(in EntityTag entityTag, ref ComponentBooleanStunning root
,ref ComponentBooleanMoving boolMoving     
,ref ComponentBooleanMoveReady boolMoveReady     
)
{

          if (entityTag.Name!=ResourceID.Bird) return;
        
          bool canMakeImpact = root.Value==  true ;

        if(canMakeImpact){

            boolMoving.Value = false; 
            boolMoveReady.Value = false; 

}

}

}




[BurstCompile]
   public partial struct StateBondJobBearRootStunningTrue : IJobEntity
{
 
        void Execute(in EntityTag entityTag, ref ComponentBooleanStunning root
,ref ComponentBooleanMoving boolMoving     
,ref ComponentBooleanMoveReady boolMoveReady     
)
{

          if (entityTag.Name!=ResourceID.Bear) return;
        
          bool canMakeImpact = root.Value==  true ;

        if(canMakeImpact){

            boolMoving.Value = false; 
            boolMoveReady.Value = false; 

}

}

}





// TRIGGER BONDS ......................................


[BurstCompile]
   public partial struct StateBondJobBirdRootTriggerForceSit : IJobEntity
{
 
        void Execute(in EntityTag entityTag, ref ComponentTriggerForceSit root
,ref ComponentBooleanMoveReady boolMoveReady     
,ref ComponentBooleanMoving boolMoving     
,ref ComponentBooleanTheGround boolTheGround     
,ref ComponentBooleanStunning boolStunning     
)
{

          if (entityTag.Name!=ResourceID.Bird) return;
        
          bool canMakeImpact = root.Value;

        if(canMakeImpact){

            boolMoveReady.Value = true; 
            boolMoving.Value = true; 
            boolTheGround.Value = true; 
            boolStunning.Value = false; 

}
        root.Value=false;

}

}
[BurstCompile]
   public partial struct StateBondJobBirdRootTriggerForceFly : IJobEntity
{
 
        void Execute(in EntityTag entityTag, ref ComponentTriggerForceFly root
,ref ComponentBooleanMoveReady boolMoveReady     
,ref ComponentBooleanMoving boolMoving     
,ref ComponentBooleanStunning boolStunning     
)
{

          if (entityTag.Name!=ResourceID.Bird) return;
        
          bool canMakeImpact = root.Value;

        if(canMakeImpact){

            boolMoveReady.Value = true; 
            boolMoving.Value = true; 
            boolStunning.Value = false; 

}
        root.Value=false;

}

}




[BurstCompile]
   public partial struct StateBondJobBearRootTriggerForceFly : IJobEntity
{
 
        void Execute(in EntityTag entityTag, ref ComponentTriggerForceFly root
,ref ComponentBooleanMoveReady boolMoveReady     
,ref ComponentBooleanMoving boolMoving     
)
{

          if (entityTag.Name!=ResourceID.Bear) return;
        
          bool canMakeImpact = root.Value;

        if(canMakeImpact){

            boolMoveReady.Value = true; 
            boolMoving.Value = true; 

}
        root.Value=false;

}

}



}