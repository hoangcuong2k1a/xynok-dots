using Rukhanka;
using Unity.Burst;
using Unity.Entities;
using Xynok.DOTs.Components.Generated;

namespace Xynok.DOTs.Systems.AnimBinding
{


//......................... bools
 [BurstCompile]
   public partial struct BindingAnimBooleanStateJob0 : IJobEntity
    {

    public   FastAnimatorParameter NoneBoolParam;

    public   FastAnimatorParameter MoveReadyBoolParam;

    public   FastAnimatorParameter MovingBoolParam;

    public   FastAnimatorParameter TheGroundBoolParam;

    public   FastAnimatorParameter CanContainBoolParam;

    public   FastAnimatorParameter SelectedBoolParam;

    public   FastAnimatorParameter StunningBoolParam;


// execute methods
 void Execute(in AnimatorControllerParameterIndexTableComponent paramIndexTable,
            ref DynamicBuffer<AnimatorControllerParameterComponent> allParams

//  params

    ,in ComponentBooleanNone  NoneBoolData
   

    ,in ComponentBooleanMoveReady  MoveReadyBoolData
   

    ,in ComponentBooleanMoving  MovingBoolData
   

    ,in ComponentBooleanTheGround  TheGroundBoolData
   

    ,in ComponentBooleanCanContain  CanContainBoolData
   

    ,in ComponentBooleanSelected  SelectedBoolData
   

    ,in ComponentBooleanStunning  StunningBoolData
   

){

  var t = paramIndexTable.seedTable;

   NoneBoolParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue = NoneBoolData.Value });
   

   MoveReadyBoolParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue = MoveReadyBoolData.Value });
   

   MovingBoolParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue = MovingBoolData.Value });
   

   TheGroundBoolParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue = TheGroundBoolData.Value });
   

   CanContainBoolParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue = CanContainBoolData.Value });
   

   SelectedBoolParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue = SelectedBoolData.Value });
   

   StunningBoolParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue = StunningBoolData.Value });
   
      
     
}
   
}

//......................... floats
 [BurstCompile]
   public partial struct BindingAnimFloatStateJob0 : IJobEntity
    {

    public   FastAnimatorParameter NoneFloatParam;

    public   FastAnimatorParameter IDFloatParam;

    public   FastAnimatorParameter MoveSpeedFloatParam;

    public   FastAnimatorParameter JumpForceFloatParam;

    public   FastAnimatorParameter JumpHoldDurationFloatParam;

    public   FastAnimatorParameter JumpHoldForceFloatParam;

    public   FastAnimatorParameter MaxJumpFloatParam;

    public   FastAnimatorParameter RadiusCollisionCheckFloatParam;

    public   FastAnimatorParameter JumpsLeftFloatParam;

    public   FastAnimatorParameter HpFloatParam;


// execute methods
 void Execute(in AnimatorControllerParameterIndexTableComponent paramIndexTable,
            ref DynamicBuffer<AnimatorControllerParameterComponent> allParams

//  params

    ,in ComponentFloatNone  NoneFloatData
   

    ,in ComponentFloatID  IDFloatData
   

    ,in ComponentFloatMoveSpeed  MoveSpeedFloatData
   

    ,in ComponentFloatJumpForce  JumpForceFloatData
   

    ,in ComponentFloatJumpHoldDuration  JumpHoldDurationFloatData
   

    ,in ComponentFloatJumpHoldForce  JumpHoldForceFloatData
   

    ,in ComponentFloatMaxJump  MaxJumpFloatData
   

    ,in ComponentFloatRadiusCollisionCheck  RadiusCollisionCheckFloatData
   

    ,in ComponentFloatJumpsLeft  JumpsLeftFloatData
   

    ,in ComponentFloatHp  HpFloatData
   

){

  var t = paramIndexTable.seedTable;

   NoneFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = NoneFloatData.Value });
   

   IDFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = IDFloatData.Value });
   

   MoveSpeedFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = MoveSpeedFloatData.Value });
   

   JumpForceFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = JumpForceFloatData.Value });
   

   JumpHoldDurationFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = JumpHoldDurationFloatData.Value });
   

   JumpHoldForceFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = JumpHoldForceFloatData.Value });
   

   MaxJumpFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = MaxJumpFloatData.Value });
   

   RadiusCollisionCheckFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = RadiusCollisionCheckFloatData.Value });
   

   JumpsLeftFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = JumpsLeftFloatData.Value });
   

   HpFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = HpFloatData.Value });
   
      
     
}
}
   
 [BurstCompile]
   public partial struct BindingAnimFloatStateJob1 : IJobEntity
    {

    public   FastAnimatorParameter MaxAttackCountFloatParam;

    public   FastAnimatorParameter AttackCountFloatParam;

    public   FastAnimatorParameter AttackHoldDurationFloatParam;

    public   FastAnimatorParameter MaxHpFloatParam;

    public   FastAnimatorParameter RageFloatParam;

    public   FastAnimatorParameter MaxRageFloatParam;

    public   FastAnimatorParameter CooldownFloatParam;

    public   FastAnimatorParameter DamageFloatParam;

    public   FastAnimatorParameter AttackSpeedFloatParam;

    public   FastAnimatorParameter RandomMoveRangeFloatParam;


// execute methods
 void Execute(in AnimatorControllerParameterIndexTableComponent paramIndexTable,
            ref DynamicBuffer<AnimatorControllerParameterComponent> allParams

//  params

    ,in ComponentFloatMaxAttackCount  MaxAttackCountFloatData
   

    ,in ComponentFloatAttackCount  AttackCountFloatData
   

    ,in ComponentFloatAttackHoldDuration  AttackHoldDurationFloatData
   

    ,in ComponentFloatMaxHp  MaxHpFloatData
   

    ,in ComponentFloatRage  RageFloatData
   

    ,in ComponentFloatMaxRage  MaxRageFloatData
   

    ,in ComponentFloatCooldown  CooldownFloatData
   

    ,in ComponentFloatDamage  DamageFloatData
   

    ,in ComponentFloatAttackSpeed  AttackSpeedFloatData
   

    ,in ComponentFloatRandomMoveRange  RandomMoveRangeFloatData
   

){

  var t = paramIndexTable.seedTable;

   MaxAttackCountFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = MaxAttackCountFloatData.Value });
   

   AttackCountFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = AttackCountFloatData.Value });
   

   AttackHoldDurationFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = AttackHoldDurationFloatData.Value });
   

   MaxHpFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = MaxHpFloatData.Value });
   

   RageFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = RageFloatData.Value });
   

   MaxRageFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = MaxRageFloatData.Value });
   

   CooldownFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = CooldownFloatData.Value });
   

   DamageFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = DamageFloatData.Value });
   

   AttackSpeedFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = AttackSpeedFloatData.Value });
   

   RandomMoveRangeFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = RandomMoveRangeFloatData.Value });
   
      
     
}
}
   
 [BurstCompile]
   public partial struct BindingAnimFloatStateJob2 : IJobEntity
    {

    public   FastAnimatorParameter IdleTimeFloatParam;

    public   FastAnimatorParameter PulseForceFloatParam;

    public   FastAnimatorParameter CooldownStackComboFloatParam;

    public   FastAnimatorParameter ImmortalCooldownFloatParam;

    public   FastAnimatorParameter StunDurationFloatParam;

    public   FastAnimatorParameter DashDurationFloatParam;

    public   FastAnimatorParameter DashCooldownFloatParam;

    public   FastAnimatorParameter DashPowerFloatParam;


// execute methods
 void Execute(in AnimatorControllerParameterIndexTableComponent paramIndexTable,
            ref DynamicBuffer<AnimatorControllerParameterComponent> allParams

//  params

    ,in ComponentFloatIdleTime  IdleTimeFloatData
   

    ,in ComponentFloatPulseForce  PulseForceFloatData
   

    ,in ComponentFloatCooldownStackCombo  CooldownStackComboFloatData
   

    ,in ComponentFloatImmortalCooldown  ImmortalCooldownFloatData
   

    ,in ComponentFloatStunDuration  StunDurationFloatData
   

    ,in ComponentFloatDashDuration  DashDurationFloatData
   

    ,in ComponentFloatDashCooldown  DashCooldownFloatData
   

    ,in ComponentFloatDashPower  DashPowerFloatData
   

){

  var t = paramIndexTable.seedTable;

   IdleTimeFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = IdleTimeFloatData.Value });
   

   PulseForceFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = PulseForceFloatData.Value });
   

   CooldownStackComboFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = CooldownStackComboFloatData.Value });
   

   ImmortalCooldownFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = ImmortalCooldownFloatData.Value });
   

   StunDurationFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = StunDurationFloatData.Value });
   

   DashDurationFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = DashDurationFloatData.Value });
   

   DashCooldownFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = DashCooldownFloatData.Value });
   

   DashPowerFloatParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { floatValue = DashPowerFloatData.Value });
   
      
     
}
}
   
}



//......................... triggers
 [BurstCompile]
   public partial struct BindingAnimTriggerStateJob0 : IJobEntity
    {

    public   FastAnimatorParameter NoneTriggerParam;

    public   FastAnimatorParameter ForceSitTriggerParam;

    public   FastAnimatorParameter ForceFlyTriggerParam;

    public   FastAnimatorParameter ForceAttackTriggerParam;


// execute methods
 void Execute(in AnimatorControllerParameterIndexTableComponent paramIndexTable,
            ref DynamicBuffer<AnimatorControllerParameterComponent> allParams

//  params

    ,in ComponentTriggerNone  NoneTriggerData
   

    ,in ComponentTriggerForceSit  ForceSitTriggerData
   

    ,in ComponentTriggerForceFly  ForceFlyTriggerData
   

    ,in ComponentTriggerForceAttack  ForceAttackTriggerData
   

){

  var t = paramIndexTable.seedTable;

if (NoneTriggerData.Value)NoneTriggerParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue =true });
   

if (ForceSitTriggerData.Value)ForceSitTriggerParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue =true });
   

if (ForceFlyTriggerData.Value)ForceFlyTriggerParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue =true });
   

if (ForceAttackTriggerData.Value)ForceAttackTriggerParam.SetRuntimeParameterData(t, allParams, new ParameterValue() { boolValue =true });
   
      
     
}
   
            }




