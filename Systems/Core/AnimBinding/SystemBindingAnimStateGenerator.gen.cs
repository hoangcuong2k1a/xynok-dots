using Rukhanka;
using Unity.Entities;
using Xynok.DOTs.Systems.AnimBinding;
using Xynok.DOTs.Attributes.Systems;
namespace Xynok.DOTs.Systems.Core.AnimBinding
{

//......................... bools
     [UpdateBefore(typeof(RukhankaAnimationSystemGroup))]
    [RequireMatchingQueriesForUpdate]
  [XynokSystem(XynokSystemAttribute.Main+"SystemBindingBooleanAnimState0")]
    public partial class SystemBindingBooleanAnimState0 : SystemBase{

// variables

  private  FastAnimatorParameter _NoneBoolParam = new("None");
   

  private  FastAnimatorParameter _MoveReadyBoolParam = new("MoveReady");
   

  private  FastAnimatorParameter _MovingBoolParam = new("Moving");
   

  private  FastAnimatorParameter _TheGroundBoolParam = new("TheGround");
   

  private  FastAnimatorParameter _CanContainBoolParam = new("CanContain");
   

  private  FastAnimatorParameter _SelectedBoolParam = new("Selected");
   

  private  FastAnimatorParameter _StunningBoolParam = new("Stunning");
   


 protected override void OnUpdate()
        {
           
            BindingAnimBooleanStateJob0 bindingAnimStateJob = new BindingAnimBooleanStateJob0
            {
              
        
     NoneBoolParam =_NoneBoolParam,
   

     MoveReadyBoolParam =_MoveReadyBoolParam,
   

     MovingBoolParam =_MovingBoolParam,
   

     TheGroundBoolParam =_TheGroundBoolParam,
   

     CanContainBoolParam =_CanContainBoolParam,
   

     SelectedBoolParam =_SelectedBoolParam,
   

     StunningBoolParam =_StunningBoolParam,
   

            };

            Dependency = bindingAnimStateJob.ScheduleParallel(Dependency);
        }

}



//......................... floats
     [UpdateBefore(typeof(RukhankaAnimationSystemGroup))]
    [RequireMatchingQueriesForUpdate]
 [XynokSystem(XynokSystemAttribute.Main+"SystemBindingFloatAnimState0")]
    public partial class SystemBindingFloatAnimState0 : SystemBase{

// variables
 
  private  FastAnimatorParameter _NoneFloatParam = new("None");
   
 
  private  FastAnimatorParameter _IDFloatParam = new("ID");
   
 
  private  FastAnimatorParameter _MoveSpeedFloatParam = new("MoveSpeed");
   
 
  private  FastAnimatorParameter _JumpForceFloatParam = new("JumpForce");
   
 
  private  FastAnimatorParameter _JumpHoldDurationFloatParam = new("JumpHoldDuration");
   
 
  private  FastAnimatorParameter _JumpHoldForceFloatParam = new("JumpHoldForce");
   
 
  private  FastAnimatorParameter _MaxJumpFloatParam = new("MaxJump");
   
 
  private  FastAnimatorParameter _RadiusCollisionCheckFloatParam = new("RadiusCollisionCheck");
   
 
  private  FastAnimatorParameter _JumpsLeftFloatParam = new("JumpsLeft");
   
 
  private  FastAnimatorParameter _HpFloatParam = new("Hp");
   


 protected override void OnUpdate()
        {
           
            BindingAnimFloatStateJob0 bindingAnimStateJob = new BindingAnimFloatStateJob0
            {
              
        
     NoneFloatParam =_NoneFloatParam,
   

     IDFloatParam =_IDFloatParam,
   

     MoveSpeedFloatParam =_MoveSpeedFloatParam,
   

     JumpForceFloatParam =_JumpForceFloatParam,
   

     JumpHoldDurationFloatParam =_JumpHoldDurationFloatParam,
   

     JumpHoldForceFloatParam =_JumpHoldForceFloatParam,
   

     MaxJumpFloatParam =_MaxJumpFloatParam,
   

     RadiusCollisionCheckFloatParam =_RadiusCollisionCheckFloatParam,
   

     JumpsLeftFloatParam =_JumpsLeftFloatParam,
   

     HpFloatParam =_HpFloatParam,
   

            };

            Dependency = bindingAnimStateJob.ScheduleParallel(Dependency);
        }

}
     [UpdateBefore(typeof(RukhankaAnimationSystemGroup))]
    [RequireMatchingQueriesForUpdate]
 [XynokSystem(XynokSystemAttribute.Main+"SystemBindingFloatAnimState1")]
    public partial class SystemBindingFloatAnimState1 : SystemBase{

// variables
 
  private  FastAnimatorParameter _MaxAttackCountFloatParam = new("MaxAttackCount");
   
 
  private  FastAnimatorParameter _AttackCountFloatParam = new("AttackCount");
   
 
  private  FastAnimatorParameter _AttackHoldDurationFloatParam = new("AttackHoldDuration");
   
 
  private  FastAnimatorParameter _MaxHpFloatParam = new("MaxHp");
   
 
  private  FastAnimatorParameter _RageFloatParam = new("Rage");
   
 
  private  FastAnimatorParameter _MaxRageFloatParam = new("MaxRage");
   
 
  private  FastAnimatorParameter _CooldownFloatParam = new("Cooldown");
   
 
  private  FastAnimatorParameter _DamageFloatParam = new("Damage");
   
 
  private  FastAnimatorParameter _AttackSpeedFloatParam = new("AttackSpeed");
   
 
  private  FastAnimatorParameter _RandomMoveRangeFloatParam = new("RandomMoveRange");
   


 protected override void OnUpdate()
        {
           
            BindingAnimFloatStateJob1 bindingAnimStateJob = new BindingAnimFloatStateJob1
            {
              
        
     MaxAttackCountFloatParam =_MaxAttackCountFloatParam,
   

     AttackCountFloatParam =_AttackCountFloatParam,
   

     AttackHoldDurationFloatParam =_AttackHoldDurationFloatParam,
   

     MaxHpFloatParam =_MaxHpFloatParam,
   

     RageFloatParam =_RageFloatParam,
   

     MaxRageFloatParam =_MaxRageFloatParam,
   

     CooldownFloatParam =_CooldownFloatParam,
   

     DamageFloatParam =_DamageFloatParam,
   

     AttackSpeedFloatParam =_AttackSpeedFloatParam,
   

     RandomMoveRangeFloatParam =_RandomMoveRangeFloatParam,
   

            };

            Dependency = bindingAnimStateJob.ScheduleParallel(Dependency);
        }

}
     [UpdateBefore(typeof(RukhankaAnimationSystemGroup))]
    [RequireMatchingQueriesForUpdate]
 [XynokSystem(XynokSystemAttribute.Main+"SystemBindingFloatAnimState2")]
    public partial class SystemBindingFloatAnimState2 : SystemBase{

// variables
 
  private  FastAnimatorParameter _IdleTimeFloatParam = new("IdleTime");
   
 
  private  FastAnimatorParameter _PulseForceFloatParam = new("PulseForce");
   
 
  private  FastAnimatorParameter _CooldownStackComboFloatParam = new("CooldownStackCombo");
   
 
  private  FastAnimatorParameter _ImmortalCooldownFloatParam = new("ImmortalCooldown");
   
 
  private  FastAnimatorParameter _StunDurationFloatParam = new("StunDuration");
   
 
  private  FastAnimatorParameter _DashDurationFloatParam = new("DashDuration");
   
 
  private  FastAnimatorParameter _DashCooldownFloatParam = new("DashCooldown");
   
 
  private  FastAnimatorParameter _DashPowerFloatParam = new("DashPower");
   


 protected override void OnUpdate()
        {
           
            BindingAnimFloatStateJob2 bindingAnimStateJob = new BindingAnimFloatStateJob2
            {
              
        
     IdleTimeFloatParam =_IdleTimeFloatParam,
   

     PulseForceFloatParam =_PulseForceFloatParam,
   

     CooldownStackComboFloatParam =_CooldownStackComboFloatParam,
   

     ImmortalCooldownFloatParam =_ImmortalCooldownFloatParam,
   

     StunDurationFloatParam =_StunDurationFloatParam,
   

     DashDurationFloatParam =_DashDurationFloatParam,
   

     DashCooldownFloatParam =_DashCooldownFloatParam,
   

     DashPowerFloatParam =_DashPowerFloatParam,
   

            };

            Dependency = bindingAnimStateJob.ScheduleParallel(Dependency);
        }

}



//......................... triggers
     [UpdateBefore(typeof(RukhankaAnimationSystemGroup))]
    [RequireMatchingQueriesForUpdate]
 [XynokSystem(XynokSystemAttribute.Main+"SystemBindingTriggerAnimState0")]

    public partial class SystemBindingTriggerAnimState0 : SystemBase{

// variables
 
  private  FastAnimatorParameter _NoneTriggerParam = new("None");
   
 
  private  FastAnimatorParameter _ForceSitTriggerParam = new("ForceSit");
   
 
  private  FastAnimatorParameter _ForceFlyTriggerParam = new("ForceFly");
   
 
  private  FastAnimatorParameter _ForceAttackTriggerParam = new("ForceAttack");
   


 protected override void OnUpdate()
        {
           
            BindingAnimTriggerStateJob0 bindingAnimStateJob = new BindingAnimTriggerStateJob0
            {
              
        
     NoneTriggerParam =_NoneTriggerParam,
   

     ForceSitTriggerParam =_ForceSitTriggerParam,
   

     ForceFlyTriggerParam =_ForceFlyTriggerParam,
   

     ForceAttackTriggerParam =_ForceAttackTriggerParam,
   

            };

            Dependency = bindingAnimStateJob.ScheduleParallel(Dependency);
        }

}



}