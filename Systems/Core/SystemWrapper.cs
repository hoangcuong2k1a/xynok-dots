﻿using System;
using ALCCore.DOTs.Worlds;
using ALCCore.Enums;
using Sirenix.OdinInspector;
using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Worlds;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Dictionary;
using Xynok.OOP.Data.Core.Primitives;

namespace ALCCore.DOTs.Systems.Core
{
    [Serializable]
    public class UnmanagedSystemWrapperDict : SerializableDictionary<SystemAsAbilityType, SystemWrapper>
    {
    }

    [Serializable]
    public class SystemWrapper : IDisposable
    {
        [ReadOnly] [SerializeField] private SystemAsAbilityType systemType;
        public BoolValue running;
        public SystemHandle SystemHandle;
        public Action Disposer;

        public SystemWrapper(SystemAsAbilityType systemType)
        {
            this.systemType = systemType;
        }

        public void Dispose()
        {
            if (SystemHandle == default)
            {
                Debug.LogError($"{systemType} being disposed but SystemHandle is default");
                return;
            }

            WorldController.Instance.World.DestroySystem(SystemHandle);
            Disposer?.Invoke();
            Disposer = default;
        }
    }
}