using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Systems.SubModules;
using Xynok.DOTs.Worlds;
using Xynok.Enums;

namespace Xynok.DOTs.Systems.Generated
{
     
    public struct FactoryUnmanagedSystem
    {
        public SystemHandle SpawnSystem(SystemAsAbilityType systemAsAbilityType)
        {
            switch (systemAsAbilityType)
            {
                                case SystemAsAbilityType.TestSystem:
                   // return WorldController.Instance.World.CreateSystem<SystemTestSystem>();
                                case SystemAsAbilityType.MoveFlatWithInput:
                    return WorldController.Instance.World.CreateSystem<SystemMoveFlatWithInput>();
                                case SystemAsAbilityType.BindingComponentToRealWorld:
                    return WorldController.Instance.World.CreateSystem<SystemBindingComponentToRealWorld>();
                            }
                    Debug.LogError($"(Unmanaged System) {systemAsAbilityType} does not exist");
                    return default;
        }
    }
}
