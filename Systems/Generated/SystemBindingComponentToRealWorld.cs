using Unity.Entities;
using Xynok.DOTs.Components.Generated;
using Xynok.DOTs.Controller.Core;
using Xynok.Enums;
using Xynok.OOP.Data.Primitives;

namespace Xynok.DOTs.Systems.Generated
{
    
/// <summary>
    /// TODO: not yet decide for this system is should be generated or notk
    /// </summary>
     [DisableAutoCreation]
    [RequireMatchingQueriesForUpdate]
    public partial struct SystemBindingComponentToRealWorld: ISystem
    {

  

        public void OnUpdate(ref SystemState state)
        {
           var currentEntities = EntityDatabase.Instance.database.GetResourceActionableWrapperCollection();
             if (currentEntities == null) return;

                                  foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatNone>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.None);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatID>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.ID);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatMoveSpeed>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.MoveSpeed);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatJumpForce>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.JumpForce);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatJumpHoldDuration>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.JumpHoldDuration);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatJumpHoldForce>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.JumpHoldForce);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatMaxJump>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.MaxJump);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatRadiusCollisionCheck>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.RadiusCollisionCheck);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatJumpsLeft>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.JumpsLeft);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatHp>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.Hp);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatMaxAttackCount>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.MaxAttackCount);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatAttackCount>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.AttackCount);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatAttackHoldDuration>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.AttackHoldDuration);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatMaxHp>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.MaxHp);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatRage>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.Rage);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatMaxRage>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.MaxRage);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatCooldown>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.Cooldown);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatDamage>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.Damage);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatAttackSpeed>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.AttackSpeed);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatRandomMoveRange>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.RandomMoveRange);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatIdleTime>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.IdleTime);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatPulseForce>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.PulseForce);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatCooldownStackCombo>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.CooldownStackCombo);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatImmortalCooldown>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.ImmortalCooldown);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatStunDuration>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.StunDuration);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatDashDuration>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.DashDuration);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatDashCooldown>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.DashCooldown);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                                 foreach (var (floatComponent, entity) in
                     SystemAPI.Query<RefRO<ComponentFloatDashPower>>().WithEntityAccess())
            {
                for (int i = 0; i < currentEntities.Count; i++)
                {
                    if (currentEntities[i].Entity.Equals(entity))
                    {
                        FloatData data = currentEntities[i].GetStat(FloatDataType.DashPower);
                        if (data.Value != floatComponent.ValueRO.Value) data.Value = floatComponent.ValueRO.Value;
                        break;
                    }
                }
            }

                        }
    }
}
