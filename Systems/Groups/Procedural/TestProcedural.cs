﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Xynok.DOTs.Systems.Groups.Procedural
{
    public class TestProcedural : MonoBehaviour
    {
        public Mesh mesh;
        public Mesh[] meshArr;
        public Vector3 spawnPos;
        public Material mat;
        public bool renderMesh;
        public bool drawProceduralNow;
        public Vector3 scale;


        void CombineMesh()
        {
            // Mesh.Com
        }
        
        [Button]
        void RenderMesh()
        {
            RenderParams rp = new RenderParams(mat);
            Matrix4x4 matrixToWorld = Matrix4x4.Translate(spawnPos);
            matrixToWorld.SetTRS(matrixToWorld.GetColumn(3), Quaternion.identity, scale);
            Graphics.RenderMesh(rp, mesh, 0, matrixToWorld);
        }

        [Button]
        void DrawProceduralNow()
        {
            Graphics.DrawProceduralNow(MeshTopology.Triangles, 0, 1);
        }

        private void Update()
        {
            if (renderMesh) RenderMesh();
            if (drawProceduralNow) DrawProceduralNow();
        }
    }
}