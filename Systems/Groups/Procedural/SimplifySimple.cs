﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityMeshSimplifier;

namespace Xynok.DOTs.Systems.Groups.Procedural
{
    [RequireComponent(typeof(MeshFilter))]
    public class SimplifySimple : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f), Tooltip("The desired quality of the simplified mesh.")]
        private float quality = 0.5f;

        [SerializeField] private MeshFilter _meshFilter;
        private Mesh _sourceMesh;

        private void Start()
        {
            Simplify();
        }

        [Button]
        private void Simplify()
        {
            if (_sourceMesh == null) _sourceMesh = _meshFilter.sharedMesh;
            Mesh sourceMesh = _sourceMesh;
          
            // Create our mesh simplifier and setup our vertices and indices from all sub meshes in it
            MeshSimplifier meshSimplifier = new ()
            {
                Vertices = sourceMesh.vertices
            };

            for (int i = 0; i < sourceMesh.subMeshCount; i++)
            {
                meshSimplifier.AddSubMeshTriangles(sourceMesh.GetTriangles(i));
            }

            // This is where the magic happens, lets simplify!
            meshSimplifier.SimplifyMesh(quality);

            // Create our new mesh and transfer vertices and indices from all sub meshes
            var newMesh = new Mesh
            {
                subMeshCount = meshSimplifier.SubMeshCount,
                vertices = meshSimplifier.Vertices
            };

            for (int i = 0; i < meshSimplifier.SubMeshCount; i++)
            {
                newMesh.SetTriangles(meshSimplifier.GetSubMeshTriangles(i), 0);
            }

            _meshFilter.sharedMesh = newMesh;
        }
    }
}