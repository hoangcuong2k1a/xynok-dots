﻿using Unity.Entities;
using Unity.Transforms;

namespace Xynok.DOTs.Systems.Groups
{
    [UpdateBefore(typeof(TransformSystemGroup))]
    public partial class XynokSimulationSystemGroup: ComponentSystemGroup
    {
        
    }
}