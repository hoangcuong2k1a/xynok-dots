using Unity.Entities;
using Unity.Physics;
using Xynok.Enums;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.APIs
{

    public interface IPhysicSimulationEvent<T> : IBufferElementData, ISimulationEvent<T>
    {
        public PhysicState State { get; set; }
    }
}
