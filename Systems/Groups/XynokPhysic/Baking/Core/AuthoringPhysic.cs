﻿using Sirenix.OdinInspector;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Authoring;
using UnityEngine;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Baking.APIs;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Components;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Data;
using Xynok.Enums;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.Baking.Core
{
    [RequireComponent(typeof(PhysicsShapeAuthoring))]
    public class AuthoringPhysic : MonoBehaviour, IPhysicAuthoring
    {
        [ReadOnly] [SerializeField] PhysicsShapeAuthoring physicsShapeAuthoring;
        [ShowIf("detectEventType", PhysicDetectEventType.Collision)]   [ReadOnly] [SerializeField] PhysicsBodyAuthoring physicsBodyAuthoring;

        [SerializeField] private PhysicDetectEventType detectEventType;
        [ShowIf("detectEventType", PhysicDetectEventType.Collision)] [SerializeField]
        private bool calculateDetails;


        private void OnValidate()
        {
            physicsShapeAuthoring ??= GetComponent<PhysicsShapeAuthoring>();
            if (detectEventType == PhysicDetectEventType.Collision)
            {
                physicsBodyAuthoring = gameObject.GetComponent<PhysicsBodyAuthoring>();
                physicsBodyAuthoring ??= gameObject.AddComponent<PhysicsBodyAuthoring>();

                if (physicsShapeAuthoring.CollisionResponse !=
                    CollisionResponsePolicy.CollideRaiseCollisionEvents)
                    physicsShapeAuthoring.CollisionResponse =
                        CollisionResponsePolicy.CollideRaiseCollisionEvents;
                return;
            }

            if (detectEventType == PhysicDetectEventType.Trigger)
            {
                if (physicsShapeAuthoring.CollisionResponse !=
                    CollisionResponsePolicy.RaiseTriggerEvents)
                    physicsShapeAuthoring.CollisionResponse =
                        CollisionResponsePolicy.RaiseTriggerEvents;
                DestroyImmediate(gameObject.GetComponent<PhysicsBodyAuthoring>());
                return;
            }
        }

        public PhysicDetectEventType DetectEventType => detectEventType;

        public class AuthoringPhysicBaker : Baker<AuthoringPhysic>
        {
            public override void Bake(AuthoringPhysic authoring)
            {
                Entity entity = GetEntity(TransformUsageFlags.Dynamic);


                switch (authoring.DetectEventType)
                {
                    case PhysicDetectEventType.Trigger:
                        DefineTrigger();

                        break;

                    case PhysicDetectEventType.Collision:
                        DefineCollision();
                        break;
                }


                void DefineTrigger()
                {
                    AddComponent(entity, new TriggerDetectTag());
                    AddBuffer<PhysicTriggerEvent>(entity);
                }

                void DefineCollision()
                {
                    AddComponent(entity, new CollisionDetectTag());

                    if (authoring.calculateDetails)
                    {
                        var dynamicBufferTag = new CollisionEventDetails
                        {
                            CalculateDetails = authoring.calculateDetails
                        };

                        AddComponent(entity, dynamicBufferTag);
                    }

                    AddBuffer<PhysicCollisionEvent>(entity);
                }
            }
        }
    }
}