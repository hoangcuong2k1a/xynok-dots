﻿using Xynok.Enums;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.Baking.APIs
{
    public interface IPhysicAuthoring
    {
        PhysicDetectEventType DetectEventType { get; }
    }
}