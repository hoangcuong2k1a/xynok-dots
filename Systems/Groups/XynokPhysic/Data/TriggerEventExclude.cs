﻿using Unity.Entities;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.Data
{
    /// <summary>
    ///  If this component is added to an entity, trigger events won't be added to a dynamic buffer
    /// of that entity by the StatefulTriggerEventBufferSystem. This component is by default added to
    /// CharacterController entity, so that CharacterControllerSystem can add trigger events to
    ///    CharacterController on its own, without StatefulTriggerEventBufferSystem interference.
    /// </summary>
    public struct TriggerEventExclude : IComponentData
    {
    }
}