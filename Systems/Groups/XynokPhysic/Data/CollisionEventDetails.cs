﻿using Unity.Entities;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.Data
{
    public struct CollisionEventDetails : IComponentData
    {
        public bool CalculateDetails;
    }
}