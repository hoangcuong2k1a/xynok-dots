using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using Xynok.DOTs.Attributes.Systems;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Data;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.Core
{
    /// <summary>
    /// This system converts stream of CollisionEvents to StatefulCollisionEvents that can be stored in a Dynamic Buffer.
    /// In order for this conversion, it is required to:
    ///    1) Use the 'Collide Raise Collision Events' option of the 'Collision Response' property on a <see cref="PhysicsShapeAuthoring"/> component, and
    ///    2) Add a <see cref="StatefulCollisionEventBufferAuthoring"/> component to that entity (and select if details should be calculated or not)
    /// or, if this is desired on a Character Controller:
    ///    1) Tick the 'Raise Collision Events' flag on the <see cref="CharacterControllerAuthoring"/> component.
    /// </summary>
    [XynokSystem(XynokSystemAttribute.Main+"StatefulCollisionEventBufferSystem")]
    [UpdateInGroup(typeof(PhysicsSystemGroup))]
    [UpdateAfter(typeof(PhysicsSimulationGroup))]
    public partial struct CollisionEventBufferSystem : ISystem
    {
        private PhysicStateSimulationEventBuffers<PhysicCollisionEvent> m_StateFulEventBuffers;
        private ComponentHandles m_Handles;

        // Component that does nothing. Made in order to use a generic job. See OnUpdate() method for details.
        internal struct DummyExcludeComponent : IComponentData {};

        struct ComponentHandles
        {
            public ComponentLookup<DummyExcludeComponent> EventExcludes;
            public ComponentLookup<CollisionEventDetails> EventDetails;
            public BufferLookup<PhysicCollisionEvent> EventBuffers;

            public ComponentHandles(ref SystemState systemState)
            {
                EventExcludes = systemState.GetComponentLookup<DummyExcludeComponent>(true);
                EventDetails = systemState.GetComponentLookup<CollisionEventDetails>(true);
                EventBuffers = systemState.GetBufferLookup<PhysicCollisionEvent>(false);
            }

            public void Update(ref SystemState systemState)
            {
                EventExcludes.Update(ref systemState);
                EventBuffers.Update(ref systemState);
                EventDetails.Update(ref systemState);
            }
        }

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            m_StateFulEventBuffers = new PhysicStateSimulationEventBuffers<PhysicCollisionEvent>();
            m_StateFulEventBuffers.AllocateBuffers();
            state.RequireForUpdate<PhysicCollisionEvent>();

            m_Handles = new ComponentHandles(ref state);
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
            m_StateFulEventBuffers.Dispose();
        }

        [BurstCompile]
        public partial struct ClearCollisionEventDynamicBufferJob : IJobEntity
        {
            public void Execute(ref DynamicBuffer<PhysicCollisionEvent> eventBuffer) => eventBuffer.Clear();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            m_Handles.Update(ref state);

            state.Dependency = new ClearCollisionEventDynamicBufferJob()
                .ScheduleParallel(state.Dependency);

            m_StateFulEventBuffers.SwapBuffers();

            var currentEvents = m_StateFulEventBuffers.Current;
            var previousEvents = m_StateFulEventBuffers.Previous;

            state.Dependency = new StatefulEventCollectionJobs.
                CollectCollisionEventsWithDetails
            {
                CollisionEvents = currentEvents,
                PhysicsWorld = SystemAPI.GetSingleton<PhysicsWorldSingleton>().PhysicsWorld,
                EventDetails = m_Handles.EventDetails
            }.Schedule(SystemAPI.GetSingleton<SimulationSingleton>(), state.Dependency);


            state.Dependency = new StatefulEventCollectionJobs.
                ConvertEventStreamToDynamicBufferJob<PhysicCollisionEvent, DummyExcludeComponent>
            {
                CurrentEvents = currentEvents,
                PreviousEvents = previousEvents,
                EventBuffers = m_Handles.EventBuffers,

                UseExcludeComponent = false,
                EventExcludeLookup = m_Handles.EventExcludes
            }.Schedule(state.Dependency);
        }
    }
}
