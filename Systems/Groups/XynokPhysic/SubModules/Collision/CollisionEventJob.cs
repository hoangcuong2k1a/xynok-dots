﻿using Unity.Collections;
using Unity.Entities;
using Unity.Physics;
using UnityEngine;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Components;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.SubModules.Collision
{
    public struct CollisionEventJob: ICollisionEventsJob
    {

        [ReadOnly] public ComponentLookup<CollisionDetectTag> CollisionEventImpulseData;
        [ReadOnly] public ComponentLookup<PhysicsVelocity> PhysicsVelocityData;

        public void Execute(CollisionEvent collisionEvent)
        {
            Entity entityA = collisionEvent.EntityA;
            Entity entityB = collisionEvent.EntityB;

            bool isBodyADynamic = PhysicsVelocityData.HasComponent(entityA);
            bool isBodyBDynamic = PhysicsVelocityData.HasComponent(entityB);

            bool isBodyARepulser = CollisionEventImpulseData.HasComponent(entityA);
            bool isBodyBRepulser = CollisionEventImpulseData.HasComponent(entityB);

            if (isBodyARepulser && isBodyBDynamic)
            {
                Debug.Log($" hit something First");
                 
            }

            if (isBodyBRepulser && isBodyADynamic)
            {
                Debug.Log($" hit something Second");
            }
        }
    }
}