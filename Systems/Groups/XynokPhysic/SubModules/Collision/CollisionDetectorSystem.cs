﻿using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using Xynok.DOTs.Attributes.Systems;
using Xynok.DOTs.Systems.Groups.XynokPhysic.Components;

namespace Xynok.DOTs.Systems.Groups.XynokPhysic.SubModules.Collision
{
    
// This system applies an impulse to any dynamic that collides with a Repulsor.
// A Repulsor is defined by a PhysicsShapeAuthoring with the `Raise Collision Events` flag ticked and a
// CollisionEventImpulse behaviour added.
    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
    [UpdateAfter(typeof(PhysicsSystemGroup))]
    [XynokSystem(XynokSystemAttribute.Main+"CollisionDetectorSystem")]
    public partial struct CollisionDetectorSystem: ISystem
    {
        internal ComponentDataHandles m_ComponentDataHandles;

        internal struct ComponentDataHandles
        {
            public ComponentLookup<CollisionDetectTag> CollisionEventImpulseData;
            public ComponentLookup<PhysicsVelocity> PhysicsVelocityData;

            public ComponentDataHandles(ref SystemState systemState)
            {
                CollisionEventImpulseData = systemState.GetComponentLookup<CollisionDetectTag>(true);
                PhysicsVelocityData = systemState.GetComponentLookup<PhysicsVelocity>(true);
            }

            public void Update(ref SystemState systemState)
            {
                CollisionEventImpulseData.Update(ref systemState);
                PhysicsVelocityData.Update(ref systemState);
            }
        }

        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate(state.GetEntityQuery(ComponentType.ReadOnly<CollisionDetectTag>()));
            m_ComponentDataHandles = new ComponentDataHandles(ref state);
        }

        public void OnUpdate(ref SystemState state)
        {
            m_ComponentDataHandles.Update(ref state);

            CollisionEventJob collisionEventImpulseJob = new CollisionEventJob
            {
                CollisionEventImpulseData = m_ComponentDataHandles.CollisionEventImpulseData,
                PhysicsVelocityData = m_ComponentDataHandles.PhysicsVelocityData,
            };
            state.Dependency =
                collisionEventImpulseJob.Schedule(SystemAPI.GetSingleton<SimulationSingleton>(), state.Dependency);

        }

     
    }
}