﻿using Rukhanka;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Xynok.DOTs.Attributes.Systems;
using Xynok.DOTs.Components.Generated;

namespace Xynok.DOTs.Systems.SubModules.DeadSystems
{
    [DisableAutoCreation]
    [RequireMatchingQueriesForUpdate]
    [XynokSystem(XynokSystemAttribute.Main+"OFF " + "TestDeadSystem")]
    [UpdateBefore(typeof(PresentationSystemGroup))]
    public partial struct TestDeadSystem : ISystem
    {
        EntityQuery _animatedEntityQuery;
        EntityQuery _materialMeshEntityQuery;
        private static readonly int DissolveAmount = Shader.PropertyToID("_DissolveAmount");
        private static readonly string originShaderName = "Shader Graphs/shader_dot_dissolve";
        private static readonly int BaseColor = Shader.PropertyToID("_BaseColor");


        public void OnCreate(ref SystemState state)
        {
            _animatedEntityQuery = state.GetEntityQuery(ComponentType.ReadOnly<AnimatedSkinnedMeshComponent>());
            _materialMeshEntityQuery = state.GetEntityQuery(ComponentType.ReadOnly<MaterialMeshInfo>(),
                ComponentType.ReadOnly<Parent>());
        }

        public void OnUpdate(ref SystemState state)
        {
            // if (Input.GetKeyDown(KeyCode.Space))
            // {
            EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
            var animateds =
                _animatedEntityQuery.ToComponentDataArray<AnimatedSkinnedMeshComponent>(Allocator.TempJob);
            var materialMeshParents = _materialMeshEntityQuery.ToComponentDataArray<Parent>(Allocator.TempJob);
            var materialMeshEntities = _materialMeshEntityQuery.ToEntityArray(Allocator.TempJob);

            for (int i = 0; i < animateds.Length; i++)
            {
                for (int j = 0; j < materialMeshParents.Length; j++)
                {
                    var entity = animateds[j].animatedRigEntity;

                    if (!ExistNegativeHp(ref state, entity)) break;

                    if (MatchedAnimatorAndRenderMesh(animateds[i], materialMeshParents[j]))
                    {
                        // get materials check
                        var meshArr =
                            state.EntityManager.GetSharedComponentManaged<RenderMeshArray>(materialMeshEntities[j]);
                        var matMeshInfo =
                            state.EntityManager.GetComponentData<MaterialMeshInfo>(materialMeshEntities[j]);
                        for (int k = 0; k < meshArr.Materials.Length; k++)
                        {
                            Material mat = meshArr.GetMaterial(matMeshInfo);

                            if (!mat.shader.name.Equals(originShaderName)) break;

                            if (mat.name != "Material.001 (Instance)"+materialMeshEntities[j])
                            {
                                Material newMat = new Material(mat)
                                {
                                    name = "Material.001 (Instance)"+materialMeshEntities[j]
                                };
                                meshArr.Materials[k] = newMat;
                                mat = newMat;
                                ecb.SetSharedComponentManaged(materialMeshEntities[j], meshArr);
                                // ecb.Playback(state.EntityManager);
                            }

                            if (mat.shader.name.Equals(originShaderName))
                            {
                                Dissolve(ref state, mat);
                            }
                        }
                    }
                }
            }

            animateds.Dispose();
            materialMeshParents.Dispose();
            materialMeshEntities.Dispose();
            ecb.Dispose();
            // }
        }

        bool MatchedAnimatorAndRenderMesh(AnimatedSkinnedMeshComponent anim, Parent parent)
        {
            return anim.rootBoneEntity == parent.Value;
        }

        bool ExistNegativeHp(ref SystemState state, Entity entity)
        {
            bool exist = state.EntityManager.HasComponent<ComponentFloatHp>(entity);
            if (!exist) return false;

            var hp = state.EntityManager.GetComponentData<ComponentFloatHp>(entity);

            return hp.Value < .001f;
        }

        void Dissolve(ref SystemState state, Material material)
        {
            // var dissolveAmount = material.GetFloat(DissolveAmount);
            // if (dissolveAmount < 1f)
            // {
            //     material.SetFloat(DissolveAmount, dissolveAmount + SystemAPI.Time.DeltaTime);
            //     return;
            // }
            //
            // material.SetFloat(DissolveAmount, 0);
            material.SetColor(BaseColor, Color.red);
        }
    }
}