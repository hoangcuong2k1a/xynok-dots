﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using Xynok.DOTs.Attributes.Systems;
using Xynok.DOTs.Components.Generated;
using Xynok.DOTs.Entities.Generated;
using Xynok.DOTs.Systems.Groups;

namespace Xynok.DOTs.Systems.SubModules
{
    // [BurstCompile]
    [XynokSystem(XynokSystemAttribute.Main + "Move Flat With Input")]
    [UpdateInGroup(typeof(XynokSimulationSystemGroup))]
    public partial struct SystemMoveFlatWithInput : ISystem
    {


        // [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            float xAxis = Input.GetAxis("Horizontal");
            float yAxis = Input.GetAxis("Vertical");
            float deltaTime = SystemAPI.Time.DeltaTime;
            foreach (var (transform, moveSpeed, canMove, velocity, moving) in SystemAPI
                         .Query<RefRW<LocalTransform>, RefRO<ComponentFloatMoveSpeed>,
                             RefRO<ComponentBooleanMoveReady>, RefRW<PhysicsVelocity>, RefRW<ComponentBooleanMoving>>()
                         .WithAll<EntityTagMoveFlatWithInput>())
            {
                float3 direction = new float3(xAxis, 0, yAxis);
                bool inputValid = direction.x != 0 || direction.z != 0;

                if (canMove.ValueRO.Value && inputValid)
                {
                    var quaternionBelongDirection = quaternion.LookRotation(direction, math.up());

                    transform.ValueRW.Position += direction * moveSpeed.ValueRO.Value * deltaTime;

                    velocity.ValueRW.Linear = float3.zero;
                    velocity.ValueRW.Angular = float3.zero;
                    transform.ValueRW.Rotation = quaternionBelongDirection;
                    moving.ValueRW.Value = true;
                    continue;
                }

                if (moving.ValueRW.Value) moving.ValueRW.Value = false;
            }

        }

    }
}