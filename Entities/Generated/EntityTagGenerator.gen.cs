using Unity.Entities;

namespace Xynok.DOTs.Entities.Generated
{


    public struct EntityTagTestSystem : IComponentData
    {
    }
   

    public struct EntityTagMoveFlatWithInput : IComponentData
    {
    }
   

    public struct EntityTagBindingComponentToRealWorld : IComponentData
    {
    }
   

    public struct EntityTagMoveAsPath : IComponentData
    {
        public Entity CurrentTarget;
    }
   


}