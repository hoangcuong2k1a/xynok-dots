﻿using System;
using Unity.Entities;
using UnityEngine;
using Xynok.Enums;

namespace Xynok.DOTs.Entities
{
    [Serializable]
    public struct EntityTag : IComponentData
    {
        [SerializeField] private ResourceID resourceID;
        public ResourceID Name => resourceID;

        public EntityTag(ResourceID resourceID)
        {
            this.resourceID = resourceID;
        }
    }
}