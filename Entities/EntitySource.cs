﻿using Unity.Entities;

namespace Xynok.DOTs.Entities
{
    /// <summary>
    /// store an entity source
    /// </summary>
    public struct EntitySource: IComponentData
    {
        public Entity Source;
    }
}