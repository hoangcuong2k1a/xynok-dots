﻿using System;
using ALCCore.GUI.View.Dropdowns.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Primitives;

namespace Xynok.DOTs.Tests
{
    public class DotsGUITest_SpawnEntityOption : MonoBehaviour
    {
        [SerializeField] private Vector3Data offsetSpawn;
        [SerializeField] private DropdownBase dropdownBase;
        [SerializeField] private ResourceID[] resourceIds = new[] { ResourceID.Bear, ResourceID.Bird };

        private void Start()
        {
            DropdownDependency dropdownDependency = new();

            dropdownDependency.Options = new string[resourceIds.Length];
            for (int i = 0; i < resourceIds.Length; i++)
            {
                dropdownDependency.Options[i] = resourceIds[i].ToString();
            }

            dropdownDependency.OnValueChanged = OnChangedOption;

            dropdownBase.SetDependency(dropdownDependency);
            OnChangedOption(0);
            
            UpdateOffsetSpawn(offsetSpawn.Value);
            offsetSpawn.AddListener(UpdateOffsetSpawn);
        }

        void UpdateOffsetSpawn(Vector3 vector3)
        {
            TesterCoreDots.Instance.offsetSpawn = vector3;
            
        }
        void OnChangedOption(int index)
        {
            TesterCoreDots.Instance.spawnObj = resourceIds[index];
        }

        private void OnDestroy()
        {
           offsetSpawn?.RemoveAllListener();
           dropdownBase.Dispose();
        }
    }
}