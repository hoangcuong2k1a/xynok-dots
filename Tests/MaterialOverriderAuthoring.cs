using Unity.Entities;
using UnityEngine;

namespace Xynok.DOTs.Tests
{
    public class MaterialOverriderAuthoring: MonoBehaviour
    {
        public class MaterialOverriderBaker: Baker<MaterialOverriderAuthoring>
        {
            public override void Bake(MaterialOverriderAuthoring authoring)
            {
                var e = GetEntity(authoring, TransformUsageFlags.Dynamic);
                AddComponent(e, new MaterialOverrider{Value = 0});
            }
        }
    }
}