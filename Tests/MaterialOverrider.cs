using Unity.Entities;
using Unity.Rendering;
using UnityEngine;
using Xynok.DOTs.Attributes.Systems;

namespace Xynok.DOTs.Tests
{
    [MaterialProperty("_DissolveAmount")]
    public struct MaterialOverrider : IComponentData
    {
        public float Value;
    }

    [XynokSystem("Main AnimateMaterialSystem")]
    public partial struct AnimateMaterialSystem : ISystem
    {
        public void OnUpdate(ref SystemState state)
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                foreach (var (mat, entity) in SystemAPI.Query<RefRW<MaterialOverrider>>().WithEntityAccess())
                {
                    mat.ValueRW.Value += SystemAPI.Time.DeltaTime;
                }
            }
        }
    }
}