﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Dictionary;
using Xynok.Patterns.Singleton;

namespace Xynok.DOTs.Tests
{
    [Flags]
    public enum TestFlagEnum
    {
        None = 0,
        Chicken = 1 << 0,
        Duck = 1 << 1,
        Bird = 1 << 2,
        Cow = 1 << 3,
        Dog = 1 << 4,
        Cat = 1 << 5,
        AnimalFlyable = Chicken | Duck | Bird,
        AnimalRunAble = Cow | Dog | Cat,
    }

    [Serializable]
    public class AStateControl
    {
        public BoolDataType root;
        public StateChanging onTrue;
        public StateChanging onFalse;
    }

    [Serializable]
    public class StateChanging: SerializableDictionary<BoolDataType,bool>
    {
        
    }
    public class Logger : ASingleton<Logger>
    {
        
        public AStateControl[] aStateControl;
      
        [Button]
        void CheckFlags(TestFlagEnum testFlagEnum = TestFlagEnum.Chicken)
        {
            Debug.Log(testFlagEnum.ToString() + ": " + (int)testFlagEnum);
            Debug.Log($"AnimalFlyable {testFlagEnum}: {(testFlagEnum & TestFlagEnum.AnimalFlyable) == testFlagEnum}");
            Debug.Log($"AnimalRunAble {testFlagEnum}: {(testFlagEnum & TestFlagEnum.AnimalRunAble) == testFlagEnum}");
        }


    }
}