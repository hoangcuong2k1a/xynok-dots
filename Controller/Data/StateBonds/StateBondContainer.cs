﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using Xynok.DOTs.Controller.Core;
using Xynok.DOTs.Controller.Data.StateBonds.Core;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Utils;
using Xynok.Utils;

namespace Xynok.DOTs.Controller.Data.StateBonds
{
    public class StateBondContainer : SerializedMonoBehaviour
    {
#if UNITY_EDITOR

        [TableMatrix(HorizontalTitle = "state root", VerticalTitle = "impacts", DrawElementMethod = "DrawElement")]
        [SerializeField]
        private StateData[,] _stateBondImpact =
            new StateData[BoolDataTypeExtensions.Length, BoolDataTypeExtensions.Length];

        [TableMatrix(HorizontalTitle = "trigger root", VerticalTitle = "impacts", DrawElementMethod = "DrawElement")]
        [SerializeField]
        private StateData[,] _triggerStateBondImpact =
            new StateData[TriggerTypeExtensions.Length, BoolDataTypeExtensions.Length];
#endif

        [SerializeField] private StateBondDict stateBondDict;
        [SerializeField] private TriggerStateBondDict triggerStateBondDict;


        public void PushBondData(ResourceID resourceID)
        {
            EntityDatabase entityDatabase = FindObjectOfType<EntityDatabase>();

            if (!entityDatabase)
            {
                Debug.LogError("EntityDatabase does not exist !");
                return;
            }

            BondContainer bondDataContainer = new BondContainer
            {
                stateBondDict = stateBondDict.Clone(),
                triggerStateBondDict = triggerStateBondDict.Clone()
            };
            if (!entityDatabase.database.entityBondDict.ContainsKey(resourceID))
                entityDatabase.database.entityBondDict.Add(resourceID, bondDataContainer);
            else entityDatabase.database.entityBondDict[resourceID] = bondDataContainer;
        }
#if UNITY_EDITOR

        [Button]
        void DrawMatrix()
        {
            DrawMatrixStateBond();
            DrawMatrixTriggerStateBond();
        }

        [Button]
        void DrawDependencies()
        {
            DrawDependenciesOfStateRoot();
            DrawDependenciesOfTriggerStateRoot();
        }


        void DrawMatrixTriggerStateBond()
        {
            BoolDataType[] boolDataTypes = EnumHelper.GetAllValuesOf<BoolDataType>();
            TriggerType[] triggerTypes = EnumHelper.GetAllValuesOf<TriggerType>();

            _triggerStateBondImpact = new StateData[triggerTypes.Length, boolDataTypes.Length];

            for (int column = 0; column < triggerTypes.Length; column++)
            {
                _triggerStateBondImpact[column, 0].label = triggerTypes[column].ToString();
                _triggerStateBondImpact[column, 0].isTitle = true;

                if (column > 0) continue;

                for (int row = 1; row < boolDataTypes.Length; row++)
                {
                    _triggerStateBondImpact[column, row].label =
                        column != 0 ? boolDataTypes[^row].ToString() : boolDataTypes[row].ToString();

                    _triggerStateBondImpact[column, row].isTitle = true;
                }
            }
        }

        void DrawMatrixStateBond()
        {
            BoolDataType[] boolDataTypes = EnumHelper.GetAllValuesOf<BoolDataType>();
            _stateBondImpact = new StateData[boolDataTypes.Length, boolDataTypes.Length];

            int sizeLoop = boolDataTypes.Length - 1;
            int row = sizeLoop;
            int padding = 1;
            for (int col = 0; col < boolDataTypes.Length; col++)
            {
                _stateBondImpact[col, 0].label = boolDataTypes[col].ToString();
                _stateBondImpact[0, col].label =
                    col != 0 ? boolDataTypes[^col].ToString() : boolDataTypes[col].ToString();

                _stateBondImpact[0, col].isTitle = true;
                _stateBondImpact[col, 0].isTitle = true;

                if (col >= padding) _stateBondImpact[col, row + padding].isNoneValid = true;

                row--;
            }
        }

        void DrawDependenciesOfTriggerStateRoot()
        {
            triggerStateBondDict = new();

            TriggerType[] all = EnumHelper.GetAllValuesOf<TriggerType>();
            for (int column = 1; column < all.Length; column++)
            {
                var colData = MatrixUtils.GetColumn(_triggerStateBondImpact, column);

                TriggerStateBond root = new()
                    { type = EnumHelper.ParseEnum<TriggerType>(colData[0].label), value = true };

                StateBondArr stateBondArr = new();

                List<StateBond> impacts = new();

                for (int row = 1; row < colData.Length; row++)
                {
                    bool valid = colData[row].value && !colData[row].isNoneValid && !colData[row].isTitle;
                    if (valid)
                        impacts.Add(new StateBond
                        {
                            type = EnumHelper.ParseEnum<BoolDataType>(_triggerStateBondImpact[0, row].label),
                            value = colData[row].value
                        });
                }

                stateBondArr.stateBonds = impacts.ToArray();
                if (stateBondArr.stateBonds.Length <= 0) continue;

                triggerStateBondDict.Add(root, stateBondArr);
            }
        }


        void DrawDependenciesOfStateRoot()
        {
            stateBondDict = new();

            BoolDataType[] all = EnumHelper.GetAllValuesOf<BoolDataType>();
            for (int column = 1; column < all.Length; column++)
            {
                var colData = MatrixUtils.GetColumn(_stateBondImpact, column);

                StateBond root = new() { type = EnumHelper.ParseEnum<BoolDataType>(colData[0].label) };

                StateBondArr stateBondArr = new();

                List<StateBond> impacts = new();

                for (int row = 1; row < colData.Length; row++)
                {
                    bool valid = colData[row].value && !colData[row].isNoneValid && !colData[row].isTitle;
                    if (valid)
                        impacts.Add(new StateBond
                        {
                            type = EnumHelper.ParseEnum<BoolDataType>(_stateBondImpact[0, row].label),
                            value = false //colData[row].value
                        });
                }

                stateBondArr.stateBonds = impacts.ToArray();
                if (stateBondArr.stateBonds.Length <= 0) continue;

                stateBondDict.Add(root, stateBondArr);
            }
        }


        static StateData DrawElement(Rect rect, StateData data)
        {
            bool isTitleField = data.isTitle;
            bool isValueField = !isTitleField;
            bool noneValid = data.isNoneValid;

            if (noneValid) return data;

            if (isValueField)
            {
                if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
                {
                    data.value = !data.value;
                    UnityEngine.GUI.changed = true;
                    Event.current.Use();
                }

                UnityEditor.EditorGUI.DrawRect(rect.Padding(1),
                    data.value ? new Color(0.4f, 0.8f, 1) : new Color(0, 0, 0, 0.5f));
            }

            if (isTitleField)
            {
                UnityEditor.EditorGUI.LabelField(rect, data.label.ToString(), new GUIStyle
                {
                    alignment = TextAnchor.MiddleCenter,
                    fontSize = 11,
                    normal = new GUIStyleState
                    {
                        textColor = Color.white
                    }
                });
            }

            return data;
        }
#endif
    }
}