﻿using System;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Dictionary;

namespace Xynok.DOTs.Controller.Data.StateBonds.Core
{
    [Serializable]
    public class EntityBondDict : SerializableDictionary<ResourceID, BondContainer>
    {
    }

    [Serializable]
    public class BondContainer
    {
        public StateBondDict stateBondDict;
        public TriggerStateBondDict triggerStateBondDict;
    }

    [Serializable]
    public class StateBondDict : SerializableDictionary<StateBond, StateBondArr>
    {
        public StateBondDict Clone()
        {
            StateBondDict clone = new StateBondDict();
            for (int i = 0; i < keys.Count; i++)
            {
                StateBond key = keys[i];
                StateBondArr value = new StateBondArr();
                StateBond[] stateBonds = new StateBond[values[i].stateBonds.Length];
                
                for (int j = 0; j < values[i].stateBonds.Length; j++)
                {
                    stateBonds[j] = values[i].stateBonds[j];
                }
                value.stateBonds = stateBonds;

                clone.Add(key, value);
            }

            return clone;
        }
    }

    [Serializable]
    public class TriggerStateBondDict : SerializableDictionary<TriggerStateBond, StateBondArr>
    {
        public TriggerStateBondDict Clone()
        {
            TriggerStateBondDict clone = new TriggerStateBondDict();
            for (int i = 0; i < keys.Count; i++)
            {
                TriggerStateBond key = keys[i];
                StateBondArr value = new StateBondArr();
                StateBond[] stateBonds = new StateBond[values[i].stateBonds.Length];
                for (int j = 0; j < values[i].stateBonds.Length; j++)
                {
                    stateBonds[j] = values[i].stateBonds[j];
                }
                value.stateBonds = stateBonds;
                clone.Add(key, value);
            }

            return clone;
        }
    }

    [Serializable]
    public struct StateData
    {
        public string label;
        public bool value;
        public bool isTitle;
        public bool isNoneValid;
    }

    [Serializable]
    public class StateBondArr
    {
        public StateBond[] stateBonds;
    }

    [Serializable]
    public struct StateBond
    {
        public BoolDataType type;
        public bool value;
    }

    [Serializable]
    public struct TriggerStateBond
    {
        public TriggerType type;
        public bool value;
    }
}