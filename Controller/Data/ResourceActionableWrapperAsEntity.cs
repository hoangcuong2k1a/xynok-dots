﻿using System;
using ALCCore.Data.Core.Collections;
using ALCCore.Enums;
using Unity.Entities;
using Xynok.Enums;
using Xynok.OOP.Data.Core.SubModules;
using Xynok.OOP.Data.Primitives;

namespace Xynok.DOTs.Controller.Data
{
    [Serializable]
    public class ResourceActionableWrapperCollection : ACollectionDataBinding<ResourceActionableWrapperAsEntity>, IComponentData
    {
    }

    /// <summary>
    /// resource is converted to entity data
    /// </summary>
    [Serializable]
    public class ResourceActionableWrapperAsEntity : ResourceActionable
    {
        public Entity Entity;

        public ResourceActionableWrapperAsEntity(ResourceID resourceID, ResourceType resourceType) : base(resourceID,
            resourceType)
        {
        }

        public void CloneFrom(ResourceActionable resourceBase)
        {
            resourceID = resourceBase.resourceID;
            resourceType = resourceBase.resourceType;

            // stats
            stats = new FloatData[resourceBase.stats.Length];
            for (int i = 0; i < resourceBase.stats.Length; i++)
            {
                stats[i] = new(resourceBase.stats[i].Key, resourceBase.stats[i].Value);
            }

            // states
            states = new BoolData[resourceBase.states.Length];
            for (int i = 0; i < resourceBase.states.Length; i++)
            {
                states[i] = new(resourceBase.states[i].Key, resourceBase.states[i].Value);
            }
            
            
            // triggers
            triggers = new TriggerData[resourceBase.triggers.Length];
            for (int i = 0; i < resourceBase.triggers.Length; i++)
            {
                triggers[i] = new(resourceBase.triggers[i].Key, resourceBase.triggers[i].Value);
            }

            // abilities
            abilities = new AbilityCollection();
            for (int i = 0; i < resourceBase.abilities.Count; i++)
            {
                abilities.Add(resourceBase.abilities[i]);
            }

            // abilities as system
            systemAsAbilities = new SystemAsAbilityCollection();
            for (int i = 0; i < resourceBase.systemAsAbilities.Count; i++)
            {
                systemAsAbilities.Add(resourceBase.systemAsAbilities[i]);
            }
            
            // vector2
            vector2Data = new Vector2Data[resourceBase.vector2Data.Length];
            for (int i = 0; i < resourceBase.vector2Data.Length; i++)
            {
                vector2Data[i] = new(resourceBase.vector2Data[i].Key, resourceBase.vector2Data[i].Value);
            }
            
            // vector3
            vector3Data = new Vector3Data[resourceBase.vector3Data.Length];
            for (int i = 0; i < resourceBase.vector3Data.Length; i++)
            {
                vector3Data[i] = new(resourceBase.vector3Data[i].Key, resourceBase.vector3Data[i].Value);
            }
        }
    }
}