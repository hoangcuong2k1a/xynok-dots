using System;
using Xynok.Enums;

namespace Xynok.DOTs.Controller.Data.Template
{
    [Serializable]
    public abstract class ADataTemplate<T1, T2>
    {
        public T1 key;
        public T2 defaultValue;
    }

    [Serializable]
    public class FloatDataTemplate : ADataTemplate<FloatDataType, float>
    {
    }
    [Serializable]
    public class BoolDataTemplate : ADataTemplate<BoolDataType, bool>
    {
    }
    [Serializable]
    public class DataTemplateContainer
    {
        public FloatDataTemplate[] floatDataTemplates;
        public BoolDataTemplate[] boolDataTemplates;
    }
}