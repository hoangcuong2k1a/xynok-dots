﻿#if UNITY_EDITOR

using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Utils;
using Xynok.OOP.Data.Primitives;

namespace Xynok.DOTs.Controller.Data.Template
{
    public class AnimatorParamGenerator : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] EntityDatabaseSO entityDatabaseSo;

        private UnityEditor.Animations.AnimatorController _animatorController;
        private FloatData[] _stats;
        private BoolData[] _states;
        private TriggerData[] _triggers;

        [Button]
        void GenParamForAnimator(ResourceID resourceID)
        {
            if (!CanGen()) return;
            _animatorController = (UnityEditor.Animations.AnimatorController)animator.runtimeAnimatorController;
            ClearParamOfAnimator();
            FetchData(resourceID);
            InitParamForAnimator();
            FillValueForParams();
            ResetData();
        }


        bool CanGen()
        {
            if (!animator)
            {
                Debug.LogError($"{gameObject.name} has no animator");
                return false;
            }

            if (!animator.runtimeAnimatorController)
            {
                Debug.LogError($"{gameObject.name} has no animator controller");
                return false;
            }

            if (!entityDatabaseSo)
            {
                Debug.LogError($"database can not be NULL");
                return false;
            }

            return true;
        }

        void ClearParamOfAnimator()
        {
            for (int i = _animatorController.parameters.Length - 1; i > -1; i--)
            {
                int index = i;
                _animatorController.RemoveParameter(index);
            }
        }

        void InitParamForAnimator()
        {
            FloatDataType[] allFloats = EnumHelper.GetAllValuesOf<FloatDataType>();
            for (int i = 0; i < allFloats.Length; i++)
            {
                if (allFloats[i] != FloatDataType.None)
                    _animatorController.AddParameter(allFloats[i].ToString(),
                        AnimatorControllerParameterType.Float);
            }

            BoolDataType[] allBooleans = EnumHelper.GetAllValuesOf<BoolDataType>();
            for (int i = 0; i < allBooleans.Length; i++)
            {
                if (allBooleans[i] != BoolDataType.None)
                    _animatorController.AddParameter(allBooleans[i].ToString(),
                        AnimatorControllerParameterType.Bool);
            }

            TriggerType[] allTriggers = EnumHelper.GetAllValuesOf<TriggerType>();
            for (int i = 0; i < allTriggers.Length; i++)
            {
                if (allTriggers[i] != TriggerType.None)
                    _animatorController.AddParameter(allTriggers[i].ToString(),
                        AnimatorControllerParameterType.Trigger);
            }
        }

        void FetchData(ResourceID resourceID)
        {
            _stats = entityDatabaseSo.Get(resourceID).stats;
            _states = entityDatabaseSo.Get(resourceID).states;
            _triggers = entityDatabaseSo.Get(resourceID).triggers;
        }

        void ResetData()
        {
            _stats = null;
            _states = null;
            _triggers = null;
        }

        void FillValueForParams()
        {
            // stats
            for (int i = 0; i < _stats.Length; i++)
            {
                int index = i;
                FloatData statData = _stats[index];

                void UpdateAnimParam(float value)
                {
                    animator.SetFloat(statData.GetHashOfKey(), value);
                }

                UpdateAnimParam(statData.Value);
            }

            // states
            for (int i = 0; i < _states.Length; i++)
            {
                int index = i;
                BoolData stateData = _states[index];

                void UpdateAnimParam(bool value)
                {
                    animator.SetBool(stateData.GetHashOfKey(), value);
                }

                UpdateAnimParam(stateData.Value);
            }

            // triggers
            for (int i = 0; i < _triggers.Length; i++)
            {
                int index = i;
                TriggerData data = _triggers[index];

                void UpdateAnimParam(bool value)
                {
                    if (value) animator.SetTrigger(data.GetHashOfKey());
                }

                UpdateAnimParam(data.Value);
            }
        }
    }
}
#endif