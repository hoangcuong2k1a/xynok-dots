﻿using System.Linq;
using ALCCore.Enums;
using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.SubModules;
using Xynok.OOP.Data.Core.Utils;
using Xynok.OOP.Data.Primitives;

namespace Xynok.DOTs.Controller.Data.Template
{
    [CreateAssetMenu(fileName = "EntityDatabaseSO", menuName = "Xynok/Data/Database")]
    public class EntityDatabaseSO : ScriptableObject
    {
        public ResourceActionable[] entityData;
        public string saveKey = "SAVE_KEY";

        public ResourceActionable Get(ResourceID resourceID) =>
            entityData.FirstOrDefault(e => e.resourceID == resourceID);

        [Button]
        void InitAll()
        {
            FloatDataType[] allFloats = EnumHelper.GetAllValuesOf<FloatDataType>();
            BoolDataType[] allBooleans = EnumHelper.GetAllValuesOf<BoolDataType>();
            TriggerType[] allTriggers = EnumHelper.GetAllValuesOf<TriggerType>();
            ResourceID[] allResources = EnumHelper.GetAllValuesOf<ResourceID>();


            entityData = new ResourceActionable[allResources.Length];
            for (int i = 0; i < allResources.Length; i++)
            {
                ResourceActionable resourceActionable = new ResourceActionable(allResources[i], ResourceType.None);
                resourceActionable.resourceID = allResources[i];
                resourceActionable.resourceType = ResourceType.None;
                entityData[i] = resourceActionable;
                entityData[i].objType = ObjType.Entity;
                entityData[i].systemAsAbilities = new SystemAsAbilityCollection();
                entityData[i].systemAsAbilities.Add(SystemAsAbilityType.MoveFlatWithInput);

                FloatData[] floatDatas = new FloatData[allFloats.Length];
                for (int j = 0; j < allFloats.Length; j++)
                {
                    FloatData floatData = new FloatData(allFloats[j], 0);
                    floatDatas[j] = floatData;
                }


                BoolData[] boolDatas = new BoolData[allBooleans.Length];
                for (int k = 0; k < allBooleans.Length; k++)
                {
                    BoolData boolData = new BoolData(allBooleans[k], false);
                    boolDatas[k] = boolData;
                }


                TriggerData[] triggerDatas = new TriggerData[allTriggers.Length];
                for (int m = 0; m < allTriggers.Length; m++)
                {
                    TriggerData triggerData = new TriggerData(allTriggers[m], false);
                    triggerDatas[m] = triggerData;
                }

                entityData[i].stats = floatDatas;
                entityData[i].states = boolDatas;
                entityData[i].triggers = triggerDatas;
            }
        }
    }
}