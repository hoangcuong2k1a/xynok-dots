﻿using Xynok.DOTs.Controller.Ability.Root;
using Xynok.Patterns.Singleton;
using Zk1Core.Patterns.Singleton;

namespace ALCCore.DOTs.Controller.Ability.Root
{
    public class FactoryDots : ALazySingleton<FactoryDots>
    {
        public AbilityFactoryDots AbilityFactoryDots;

        void Awake()
        {
            Init();
        }

        void Init()
        {
            AbilityFactoryDots = new AbilityFactoryDots();
        }
    }
}