﻿using ALCCore.DOTs.Controller.Ability.Root;
using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Services.Abilities.Core;
using Xynok.Patterns.Factory.Reflection;

namespace Xynok.DOTs.Controller.Ability.Root
{
    public class AbilityFactoryDots : AFactory<AbilityType, AAbilityForDots>
    {
    }

    public abstract class AAbilityForDots : AAbility
    {
    }

    public class AbilityControllerDots : AbilityController
    {
        protected override void InitNewAbility(AbilityType abilityType)
        {
            bool duplicatedAbility = Abilities.Find(e => e.GetProductType() == abilityType) != null;

            if (duplicatedAbility)
            {
                Debug.LogError($"owner {Owner.GetType().Name} duplicated ability {abilityType}!");
                return;
            }

            var ability = FactoryDots.Instance.AbilityFactoryDots.GetProduct(abilityType);
            ability.SetDependency(Owner);
            OnDispose += ability.Dispose;
            Abilities.Add(ability);
        }
    }
}