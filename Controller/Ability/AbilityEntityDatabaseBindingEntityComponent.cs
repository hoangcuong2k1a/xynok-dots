﻿using System;
using UnityEngine;
using Xynok.DOTs.Components.Generated;
using Xynok.DOTs.Controller.Ability.Root;
using Xynok.DOTs.Controller.Core;
using Xynok.DOTs.Controller.Data;
using Xynok.Enums;

namespace Xynok.DOTs.Controller.Ability
{
    public class AbilityEntityDatabaseBindingEntityComponent : AAbilityForDots
    {
        protected override AbilityType AbilityType => AbilityType.EntityDatabaseBindingEntityComponent;
        private IEntityDatabase _entityDatabase;
        private Action _onDispose;

        protected override bool Matched()
        {
            if (Owner is IEntityDatabase entityDatabase)
            {
                _entityDatabase = entityDatabase;
                return true;
            }

            Debug.LogError($" {Owner.GetType()} is not an IEntityDatabase");
            return false;
        }

        protected override void Init()
        {
            _entityDatabase.GetResourceActionableWrapperCollection().OnAdd += OnAddResourceActionableWrapper;
        }

        void OnAddResourceActionableWrapper(ResourceActionableWrapperAsEntity wrapper)
        {
            BindingStats(wrapper);
            BindingStates(wrapper);
            BindingTriggers(wrapper);
            BindingVector2(wrapper);
            BindingVector3(wrapper);
        }

        /// <summary>
        /// stat changed -> component changed
        /// </summary>
        void BindingStats(ResourceActionableWrapperAsEntity wrapper)
        {
            for (int i = 0; i < wrapper.stats.Length; i++)
            {
                int index = i;
                wrapper.stats[index].AddListener(UpdateComponent);

                void UpdateComponent(float value)
                {
                    FactoryEntityComponent.SetComponent(wrapper.Entity, wrapper.stats[index]);
                }

                _onDispose += () => wrapper.stats[index].RemoveListener(UpdateComponent);
            }
        }

        /// <summary>
        /// state changed -> component changed
        /// </summary>
        void BindingStates(ResourceActionableWrapperAsEntity wrapper)
        {
            for (int i = 0; i < wrapper.states.Length; i++)
            {
                int index = i;
                wrapper.states[index].AddListener(UpdateComponent);

                void UpdateComponent(bool value)
                {
                    FactoryEntityComponent.SetComponent(wrapper.Entity, wrapper.states[index]);
                }

                _onDispose += () => wrapper.states[index].RemoveListener(UpdateComponent);
            }
        }
        
        /// <summary>
        /// trigger changed -> component changed
        /// </summary>
        void BindingTriggers(ResourceActionableWrapperAsEntity wrapper)
        {
            for (int i = 0; i < wrapper.triggers.Length; i++)
            {
                int index = i;
                wrapper.triggers[index].AddListener(UpdateComponent);

                void UpdateComponent(bool value)
                {
                    FactoryEntityComponent.SetComponent(wrapper.Entity, wrapper.triggers[index]);
                }

                _onDispose += () => wrapper.triggers[index].RemoveListener(UpdateComponent);
            }
        }

        void BindingVector2(ResourceActionableWrapperAsEntity wrapper)
        {
            for (int i = 0; i < wrapper.vector2Data.Length; i++)
            {
                int index = i;
                wrapper.vector2Data[index].AddListener(UpdateComponent);

                void UpdateComponent(Vector2 value)
                {
                    FactoryEntityComponent.SetComponent(wrapper.Entity, wrapper.states[index]);
                }

                _onDispose += () => wrapper.vector2Data[index].RemoveListener(UpdateComponent);
            }
        }

        void BindingVector3(ResourceActionableWrapperAsEntity wrapper)
        {
            for (int i = 0; i < wrapper.vector3Data.Length; i++)
            {
                int index = i;
                wrapper.vector3Data[index].AddListener(UpdateComponent);

                void UpdateComponent(Vector3 value)
                {
                    FactoryEntityComponent.SetComponent(wrapper.Entity, wrapper.states[index]);
                }

                _onDispose += () => wrapper.vector3Data[index].RemoveListener(UpdateComponent);
            }
        }

        protected override void OnDispose()
        {
            _entityDatabase.GetResourceActionableWrapperCollection().OnAdd -= OnAddResourceActionableWrapper;
            _onDispose?.Invoke();
            _onDispose = default;
        }
    }
}