﻿using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Controller.Ability.Root;
using Xynok.DOTs.Controller.Core;
using Xynok.DOTs.Controller.Data;
using Xynok.DOTs.Entities;
using Xynok.DOTs.Worlds;
using Xynok.Enums;
using Xynok.OOP.Data.Core.SubModules;

namespace Xynok.DOTs.Controller.Ability
{
    public class AbilityEntityDatabaseAutoAddComponentForEntity : AAbilityForDots
    {
        protected override AbilityType AbilityType => AbilityType.EntityDatabaseAutoAddComponentForEntity;
        private IEntityDatabase _entityDatabase;

        protected override void Init()
        {
            _entityDatabase.EntityCollection.OnAdd += OnAddEntity;
        }

        void OnAddEntity(Entity entity)
        {
            var tag = WorldController.Instance.EntityManager.GetComponentData<EntityTag>(entity);

            ResourceActionable source =_entityDatabase.GetEntityData(tag.Name);

            ResourceActionableWrapperAsEntity wrapper =
                new ResourceActionableWrapperAsEntity(source.resourceID, source.resourceType)
                {
                    Entity = entity
                };
            wrapper.CloneFrom(source);

            WorldController.Instance.DatabaseDots.GetResourceActionableWrapperCollection().Add(wrapper);

            Debug.Log($"entity {tag.Name} added");
        }

        protected override bool Matched()
        {
            if (Owner is IEntityDatabase entityCollection)
            {
                _entityDatabase = entityCollection;
                return true;
            }

            Debug.LogError($"{Owner.GetType()} is not an IEntityCollection");
            return false;
        }


        protected override void OnDispose()
        {
            _entityDatabase.EntityCollection.OnAdd -= OnAddEntity;
        }
    }
}