﻿using System;
using UnityEngine;
using Xynok.DOTs.Controller.Ability.Root;
using Xynok.DOTs.Controller.Core;
using Xynok.DOTs.Controller.Data;
using Xynok.DOTs.Controller.Data.StateBonds.Core;
using Xynok.Enums;
using Xynok.OOP.Data.Primitives;

namespace Xynok.DOTs.Controller.Ability
{
    public class AbilityEntityDatabaseEntityStateBond : AAbilityForDots
    {
        protected override AbilityType AbilityType => AbilityType.EntityDatabaseEntityDataStateBondRealWorldSide;
        private IEntityDatabase _entityDatabase;
        private Action _onDispose;

        private EntityBondDict EntityBondDict => _entityDatabase.GetEntityBondDict();

        protected override bool Matched()
        {
            if (Owner is IEntityDatabase entityDatabase)
            {
                _entityDatabase = entityDatabase;
                return true;
            }

            Debug.LogError($" {Owner.GetType()} is not an IEntityDatabase");
            return false;
        }

        protected override void Init()
        {
            _entityDatabase.GetResourceActionableWrapperCollection().OnAdd += OnAddResourceActionableWrapper;
        }

        void OnAddResourceActionableWrapper(ResourceActionableWrapperAsEntity wrapper)
        {
            if (!CanBindBond(wrapper.resourceID)) return;
            
            BondContainer bondContainer = EntityBondDict[wrapper.resourceID];

            BindingStateBond(wrapper, bondContainer);
            BindingTriggerStateBond(wrapper, bondContainer);
        }

        bool CanBindBond(ResourceID resourceID)
        {
            if (!EntityBondDict.ContainsKey(resourceID))
            {
                Debug.LogWarning($"State Bond Dict does not contain {resourceID}");
                return false;
            }

            return true;
        }

        void BindingTriggerStateBond(ResourceActionableWrapperAsEntity wrapper, BondContainer bondContainer)
        {
            foreach (var pair in bondContainer.triggerStateBondDict)
            {
                TriggerData boolData = wrapper.GetTrigger(pair.Key.type);
                bool invokeValue = pair.Key.value;

                void StateChange(bool state)
                {
                    if (invokeValue == state)
                    {
                        for (int i = 0; i < pair.Value.stateBonds.Length; i++)
                        {
                            int index = i;
                            BoolData impactState = wrapper.GetState(pair.Value.stateBonds[index].type);
                            impactState.Value = pair.Value.stateBonds[index].value;
                        }
                    }
                }

                boolData.AddListener(StateChange);
                _onDispose += () => boolData.RemoveListener(StateChange);
            }
        }

        void BindingStateBond(ResourceActionableWrapperAsEntity wrapper, BondContainer bondContainer)
        {
            foreach (var pair in bondContainer.stateBondDict)
            {
                BoolData boolData = wrapper.GetState(pair.Key.type);
                bool invokeValue = pair.Key.value;

                void StateChange(bool state)
                {
                    if (invokeValue == state)
                    {
                        for (int i = 0; i < pair.Value.stateBonds.Length; i++)
                        {
                            int index = i;
                            BoolData impactState = wrapper.GetState(pair.Value.stateBonds[index].type);
                            impactState.Value = pair.Value.stateBonds[index].value;
                        }
                    }
                }

                boolData.AddListener(StateChange);
                _onDispose += () => boolData.RemoveListener(StateChange);
            }
        }

        protected override void OnDispose()
        {
            _entityDatabase.GetResourceActionableWrapperCollection().OnAdd -= OnAddResourceActionableWrapper;
            _onDispose?.Invoke();
            _onDispose = default;
        }
    }
}