﻿using Xynok.Patterns.Singleton;

namespace Xynok.DOTs.Controller.Core
{
    public class EntityDatabase : ASingleton<EntityDatabase>
    {
        public DatabaseDots database;
        

        protected override void Awake()
        {
            base.Awake();
            database.Start();
        }

        private void OnDestroy()
        {
          
            database.Destroy();
        }
    }
}