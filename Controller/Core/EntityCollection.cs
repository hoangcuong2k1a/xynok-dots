﻿using System;
using ALCCore.Data.Core.Collections;
using ALCCore.Entities.APIs;
using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Controller.Data;
using Xynok.DOTs.Controller.Data.StateBonds.Core;
using Xynok.Enums;
using Xynok.OOP.Data.Core.SubModules;

namespace Xynok.DOTs.Controller.Core
{
    public interface IEntityDatabase:IAbilityOwner
    {
        EntityCollection EntityCollection { get; }

        ResourceActionable GetEntityData(ResourceID resourceID);
        
        ResourceActionableWrapperCollection GetResourceActionableWrapperCollection();
        EntityBondDict GetEntityBondDict();
    }

    [Serializable]
    public class EntityCollection : ACollectionDataBinding<Entity>
    {
        public override void Add(Entity item)
        {
            if (Existed(item))
            {
                Debug.LogError($"[EntityCollection - Add]{item.ToString()} existed");
                return;
            }

            elements.Add(item);
            OnAdd?.Invoke(item);
        }

        public override bool Remove(Entity item)
        {
            if (!Existed(item))
            {
                Debug.LogError($"[EntityCollection - Remove]{item.ToString()} does not existed");
                return false;
            }

            elements.Remove(item);
            OnRemove?.Invoke(item);
            return true;
        }

        bool Existed(Entity entity)
        {
            for (int i = 0; i < elements.Count; i++)
                if (elements[i].Equals(entity))
                    return true;

            return false;
        }
    }
}