﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using Unity.Entities;
using UnityEngine;
using Xynok.DOTs.Components.Generated;
using Xynok.DOTs.Controller.Ability.Root;
using Xynok.DOTs.Controller.Data;
using Xynok.DOTs.Controller.Data.StateBonds.Core;
using Xynok.DOTs.Controller.Data.Template;
using Xynok.DOTs.Worlds;
using Xynok.Enums;
using Xynok.OOP.Data.Core.SubModules;
using Newtonsoft.Json;
namespace Xynok.DOTs.Controller.Core
{
    [Serializable]
    public class DatabaseDots : IComponentData, IEntityDatabase
    {
        [SerializeField] private bool devMode;
        [SerializeField] private EntityDatabaseSO source;
        [SerializeField] private AbilityCollection entityDatabaseAbilities;
        [SerializeField] [ReadOnly] private ResourceActionable[] resourceData;
        public EntityBondDict entityBondDict;

        [Title("Resources are converted to Entity Data")] [SerializeField] [ReadOnly]
        private ResourceActionableWrapperCollection resourceAsEntityData;

        private EntityCollection _entityCollection;

        public EntityCollection EntityCollection
        {
            get
            {
                _entityCollection ??= new();
                return _entityCollection;
            }
        }

        public ResourceActionable GetEntityData(ResourceID resourceID) =>
            resourceData.FirstOrDefault(e => e.resourceID == resourceID);

        public ResourceActionableWrapperCollection GetResourceActionableWrapperCollection() => resourceAsEntityData;
        public EntityBondDict GetEntityBondDict() => entityBondDict;

        public AbilityCollection Abilities => entityDatabaseAbilities;

        private AbilityControllerDots _abilityController;
        private Entity _entityDatabase;

        public void Start()
        {
            LoadData();
            _abilityController = new();
            _abilityController.SetDependency(this);
            InitDatabaseEntity();
        }

        void LoadData()
        {
            if (devMode)
            {
                resourceData = source.entityData;
                return;
            }

            var json = PlayerPrefs.GetString(source.saveKey);
            resourceData = JsonConvert.DeserializeObject<ResourceActionable[]>(json);
        }
        
        void SavePlayerData()
        {
            PlayerPrefs.SetString(source.saveKey, JsonConvert.SerializeObject(resourceData, Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }));
            PlayerPrefs.Save();
        }

        [Button]
        void Sync()
        {
            entityBondDict = entityBondDict;
        }

        void InitDatabaseEntity()
        {
            _entityDatabase = WorldController.Instance.EntityManager.CreateEntity();
            FactoryEntityComponent.FillUpDataFor(_entityDatabase, ResourceID.System);
            WorldController.Instance.DatabaseDots.EntityCollection.Add(_entityDatabase);
        }

        public void Destroy()
        {
            SavePlayerData();
            _abilityController?.Dispose();
        }
    }
}