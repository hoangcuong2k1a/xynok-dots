﻿using System;
using System.Linq;

namespace Xynok.DOTs.Attributes.Systems
{
    
    public class XynokSystemAttribute: Attribute
    {
        private readonly string _systemName;
        public const string Main = "[MAIN] ";
        public const string Burst = "[BURST] ";
        public XynokSystemAttribute(string systemName)
        {
            _systemName = systemName;
        }
        public static string[] GetAllSystemNames()
        {
            var assembly = typeof(XynokSystemAttribute).Assembly;
            var types = assembly.GetTypes().Where(t => t.GetCustomAttributes(typeof(XynokSystemAttribute), false).Length > 0);
            return types.Select(t => GetName(t)).ToArray();
        }

         static string GetName(Type type)
        {
            var attribute = (XynokSystemAttribute)GetCustomAttribute(type, typeof(XynokSystemAttribute));
            return attribute._systemName;
        }
    }
}